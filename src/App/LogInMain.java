package App;

import Controller.LogInController;
import View.LogInView;
import View.AdminView;
import View.CustomerView;
import View.TellerView;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JApplet;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * @author Manish
 */
public class LogInMain 
{

    /**
     * LoginMain initializes the app and displays the login screen.
     * 
     * @param args the command line arguments
     * @throws java.sql.SQLException Thrown in event of database access error
     * @throws java.lang.ClassNotFoundException Thrown in event of missing Derby class
     * @throws java.lang.InstantiationException  Thrown when an application 
     * tries to create an instance of a class using the newInstance method in class Class, 
     * but the specified class object cannot be instantiated.
     * @throws java.lang.IllegalAccessException Thrown in event of user rights issue
     * @throws javax.swing.UnsupportedLookAndFeelException Thrown in event of missing LookAndFeel info
     */
    public static void main(String[] args) throws SQLException, 
            ClassNotFoundException, InstantiationException, 
            IllegalAccessException, UnsupportedLookAndFeelException 
    {
        for (javax.swing.UIManager.LookAndFeelInfo info 
                : javax.swing.UIManager.getInstalledLookAndFeels()) 
        {
            if ("Metal".equals(info.getName())) 
            {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
           }
        }
        
         java.awt.EventQueue.invokeLater(new Runnable() 
         {
            @Override
            public void run() 
            {
                String companyName = "Hello World Bank";
                                
                LogInView loginView = new LogInView();
                loginView.setTitle(companyName);
                AdminView adminView = new AdminView();
                adminView.setTitle(companyName);
                CustomerView customerView = new CustomerView();
                customerView.setTitle(companyName);
                TellerView tellerView = new TellerView();
                tellerView.setTitle(companyName);
                                               
                try 
                {
                    LogInController login = new LogInController(loginView, adminView, customerView, tellerView);
                } 
                catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) 
                {
                    Logger.getLogger(LogInMain.class.getName()).log(Level.SEVERE, null, ex);
                }                
                
                loginView.setFocusable(true);
                loginView.setVisible(true);
            }
        });
    }
}