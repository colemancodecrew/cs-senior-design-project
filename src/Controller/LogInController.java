/**
 * Package Controller contains all module-specific Controller classes
 * @see InventoryController
 * @see MainController
 * @see OrderController
 * @see ProfileController
 * @see ReportController
 * @see UserController
 * 
 * @author Manish
 */
package Controller;

import Model.DataClasses.BankAccount;
import Model.DataClasses.Bill;
import Model.DataClasses.BillPayAccount;
import Model.DataClasses.Customer;
import Model.DataClasses.Teller;
import Model.DataClasses.Transaction;
import Model.DataClasses.User;
import Model.DatabaseAccess.DatabaseUser;
import View.LogInView;
import View.AdminView;
import View.TellerView;
import View.CustomerView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

//GlobalUser declared here, like vUser
/**
 * LoginController provides all event handling for LoginView buttons.
 * @author Manish
 * @author Rob Vary
 */
public class LogInController 
{
    private final LogInView loginView;
    private final AdminView adminView;
    private final TellerView tellerView;
    private final CustomerView customerView;
    private final DatabaseUser databaseUser;
    private ArrayList<User> users;
    private ArrayList<BankAccount> accounts;
    private ArrayList<BillPayAccount> bp_accounts;
    private ArrayList<User> usersToDelete;
    private User vUser;
        
    /**
    *
    * @param loginView LoginView contains user data required by LoginController
     * @param adminView
     * @param customerView
     * @param tellerView
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
    */
    public LogInController(LogInView loginView, AdminView adminView,CustomerView customerView, TellerView tellerView ) 
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        this.loginView = loginView;
        this.adminView = adminView;
        this.tellerView = tellerView;
        this.customerView = customerView;
        this.databaseUser = DatabaseUser.getDatabaseUser();
        this.loginView.addLoginListener(new LoginListener());
        this.loginView.addCloseListener(new CloseListener());
        this.loginView.addKeyListener(new EnterListener());
        this.loginView.getUsernameTextField().requestFocusInWindow();
        this.loginView.addWindowListener(new WindowListener());
        this.adminView.addWindowListener(new WindowListener());
        this.tellerView.addWindowListener(new WindowListener());
        this.customerView.addWindowListener(new WindowListener());
        users = databaseUser.getUsers();
        accounts = databaseUser.getAccounts();
        bp_accounts = databaseUser.getBillPayAccounts();
        usersToDelete = new ArrayList<>();
        vUser = null;
    }
        
    class LoginListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            try 
            {
                String vUserId = loginView.getUserId();
                String vUserPassword = loginView.getUserPassword();
                vUser = databaseUser.validateUser(vUserId, vUserPassword);
                
                if(vUser != null)
                {
                    String vUserType = vUser.getUserType();
                    if(vUserType.matches("Administrator"))
                    {
                        loginView.setVisible(false);
                        adminView.addLoadListener(new AdminLoadListener());
                        adminView.addSaveListener(new AdminSaveListener());
                        adminView.addDeleteListener(new AdminDeleteListener());
                        adminView.addLogoutListener(new LogoutListener());
                        adminView.setVisible(true);
                    } 
                    else if (vUserType.matches("Teller"))
                    {
                        loginView.setVisible(false);
                        tellerView.addLogout1Listener(new LogoutListener());
                        tellerView.addLogout2Listener(new LogoutListener());
                        tellerView.addOKListener(new TellerOKListener());
                        tellerView.addOKListener2(new TellerOKListener2());
                        tellerView.addTransferListener(new TellerTransferListener());
                        tellerView.addWithdrawListener(new TellerWithdrawalListener());
                        tellerView.addDepositListener(new TellerDepositListener());
                        tellerView.setVisible(true);
                    }
                    else if (vUserType.matches("Customer"))
                    {
                        loginView.setVisible(false);
                        customerView.populate((Customer)vUser);
                        customerView.addTransferListener(new CustomerTransferListener());
                        customerView.addReportsListener(new CustomerReportsListener());
                        customerView.addLogoutListener(new LogoutListener());
                        customerView.addCreateListener(new CustomerCreateListener());
                        customerView.addAddNewListener(new CustomerAddNewListener());
                        customerView.addPayListener(new CustomerPayListener());
                        customerView.setVisible(true);
                        
                        SwingWorker sw = new SwingWorker()
                        {
                            @Override
                            protected Object doInBackground() throws Exception 
                            {
                                try 
                                { 
                                    for (BankAccount account : accounts)
                                    {
                                        if (account.getUserID() == vUser.getUserID())
                                        {
                                            //Calculate the interest for the applicable accounts.
                                            if (account.getAccountType().equals("SavingsAccount") 
                                               || account.getAccountType().equals("CreditCardAccount")
                                               || account.getAccountType().equals("MoneyMarketAccount"))
                                            {
                                                account.calcInterest();
                                                databaseUser.saveOne(account);
                                            }

                                            //Check if a payment has been received for applicable accounts.
                                            if (account.getAccountType().equals("CreditCardAccount")
                                                || account.getAccountType().equals("HomeMortgageAccount"))
                                            {
                                                account.checkFeeReceived();
                                                databaseUser.saveOne(account);
                                            }      
                                        }
                                    }
                                }
                                catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) 
                                {
                                    Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                return null;
                            }
                        };
                        sw.execute();
                    }
                }
                else 
                {
                    loginView.clearFields();
                    JOptionPane.showMessageDialog(null, "Username or password is invalid!");
                }
            } 
            catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) 
            {
                Logger.getLogger(
                 LogInController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    class CloseListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            databaseUser.closeConection();
            System.exit(0);
        }
    }
    
    class EnterListener implements KeyListener 
    {
        @Override
        public void keyPressed(KeyEvent e)
        { 
            if(KeyEvent.getKeyText(e.getKeyCode()).equals("Enter"))
            {
                loginView.getLoginButton().doClick();
            }
            
            if(KeyEvent.getKeyText(e.getKeyCode()).equals("Escape"))
            {
                loginView.getCloseButton().doClick();
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        { 
            //System.out.println("Released " + KeyEvent.getKeyText(e.getKeyCode())); 
        }

        @Override
        public void keyTyped(KeyEvent e)
        { 
            //System.out.println("Typed " + KeyEvent.getKeyText(e.getKeyCode())); 
        }
    }
    
    class AdminLoadListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            adminView.getProgBar().setBorder(BorderFactory.createTitledBorder("Loading..."));
            adminView.getProgBar().setIndeterminate(true);
            adminView.getProgBar().setVisible(true);
                
            SwingWorker sw = new SwingWorker() 
            {                
                ArrayList<User> loadedUsers = new ArrayList<>();
                
                @Override
                protected Object doInBackground() throws Exception 
                {
                    try 
                    {
                        loadedUsers = databaseUser.getUsers();
                    } 
                    catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) 
                    {
                        Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }

                @Override
                public void done()
                {
                    adminView.getProgBar().setVisible(false);
                    adminView.populateTable(loadedUsers);
                }
            };
            sw.execute();
        }
    }
    
    class AdminSaveListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            adminView.getProgBar().setBorder(BorderFactory.createTitledBorder("Saving..."));
            adminView.getProgBar().setIndeterminate(true);
            adminView.getProgBar().setVisible(true);
                
            SwingWorker sw = new SwingWorker() 
            {                
                ArrayList<User> loadedUsers = new ArrayList<>();
                
                @Override
                protected Object doInBackground() throws Exception 
                {
                    try 
                    {
                        databaseUser.saveMany(adminView.getTableContents());
                        databaseUser.deleteMany(usersToDelete);
                        usersToDelete.clear();
                        loadedUsers = databaseUser.getUsers();
                    } 
                    catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) 
                    {
                        Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }

                @Override
                public void done()
                {
                    adminView.getProgBar().setVisible(false);
                    adminView.populateTable(loadedUsers);
                }
            };
            sw.execute();
        }
    }
    
    class AdminDeleteListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to delete this user?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                try 
                {
                    usersToDelete.add(adminView.getSelectedUser());
                } 
                catch (ClassNotFoundException ex) 
                {
                    Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                }
               adminView.removeFromTable();
            }
        }
    }
    
    class CustomerTransferListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            double amount = customerView.getTransferAmount();

            BankAccount fromAccount = customerView.getFromAccount();
            BankAccount toAccount = customerView.getToAccount();
            
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to transfer $" + (new DecimalFormat("#0.00")).format(amount) + " from " + fromAccount.getReadableAccountType() + " to " + toAccount.getReadableAccountType() + "?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                boolean success = ((Customer)vUser).transfer(fromAccount, toAccount, amount);
                
                if(success)
                {
                    customerView.getTransferProgBar().setBorder(BorderFactory.createTitledBorder("Saving..."));
                    customerView.getTransferProgBar().setIndeterminate(true);
                    customerView.getTransferProgBar().setVisible(true);
                }

                SwingWorker sw = new SwingWorker() 
                {                
                    @Override
                    protected Object doInBackground() throws Exception 
                    {
                        if(success)
                        {
                            ArrayList<Object> saveList = new ArrayList<>();
                            saveList.add(fromAccount);
                            saveList.add(toAccount);
                            try 
                            {
                                databaseUser.saveMany(saveList);
                                databaseUser.load();
                            } 
                            catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) 
                            {
                                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        return null;
                    }

                    @Override
                    public void done()
                    {
                        customerView.getTransferProgBar().setVisible(false);
                        if(success)
                        {
                            JOptionPane.showMessageDialog(null, "Transfer successful!");
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Transfer failure! Insufficient funds.");
                        }
                        customerView.populate((Customer)vUser);
                    }
                };
                sw.execute();
            }
            
        }
    }
    
    class CustomerCreateListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            java.lang.Class selection = customerView.getSelectedCreateAccount();
            
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to create a " + getReadableType(selection) + "?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                BankAccount newAccount = ((Customer)vUser).setupBankAccount(selection);
                
                String test = newAccount.getReadableAccountTypeForSQL();
                
                customerView.getjProgressBar_Accounts().setBorder(BorderFactory.createTitledBorder("Creating..."));
                customerView.getjProgressBar_Accounts().setIndeterminate(true);
                customerView.getjProgressBar_Accounts().setVisible(true);

                SwingWorker sw = new SwingWorker() 
                {                
                    @Override
                    protected Object doInBackground() throws Exception 
                    {
                        try 
                        {
                            databaseUser.saveOne(newAccount);
                            databaseUser.load();
                            vUser = databaseUser.getUser(vUser.getUserID());
                        } 
                        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) 
                        {
                            Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        return null;
                    }

                    @Override
                    public void done()
                    {
                        customerView.getjProgressBar_Accounts().setVisible(false);
                        customerView.populate((Customer)vUser);
                    }
                };
                sw.execute();
            }
            
        }
    }
    
    class CustomerAddNewListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            String name = customerView.getBPName();
            int acctNum = Integer.parseInt(customerView.getBPAcctNum());
            String address = customerView.getBPAddress();
            
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to add the " + name + " bill pay account?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                BillPayAccount newAccount = new BillPayAccount(name, address, acctNum, ((Customer)vUser).getUserID(), 0);
                boolean success = ((Customer)vUser).addBillPay(newAccount);
                
                if(success)
                {
                    customerView.getjProgressBar_BillPay().setBorder(BorderFactory.createTitledBorder("Creating..."));
                    customerView.getjProgressBar_BillPay().setIndeterminate(true);
                    customerView.getjProgressBar_BillPay().setVisible(true);
                }

                SwingWorker sw = new SwingWorker() 
                {                
                    @Override
                    protected Object doInBackground() throws Exception 
                    {
                        if(success)
                        {
                            try 
                            {
                                databaseUser.saveOne(newAccount);
                                databaseUser.load();
                                vUser = databaseUser.getUser(vUser.getUserID());
                            } 
                            catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) 
                            {
                                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        return null;
                    }

                    @Override
                    public void done()
                    {
                        customerView.getjProgressBar_BillPay().setVisible(false);
                        if(success)
                        {
                            JOptionPane.showMessageDialog(null, "Account added!");
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Account already exists!");
                        }
                        customerView.populate((Customer)vUser);
                    }
                };
                sw.execute();
            }
            
        }
    }
    
    class TellerTransferListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            double amount = tellerView.getTransferAmount();

            BankAccount fromAccount = tellerView.getFromAccount();
            BankAccount toAccount = tellerView.getToAccount();
            
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to transfer $" + (new DecimalFormat("#0.00")).format(amount) + " from " + fromAccount.getReadableAccountType() + " to " + toAccount.getReadableAccountType() + "?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                boolean success = tellerView.getReferencedCust().transfer(fromAccount, toAccount, amount);
                
                if(success)
                {
                    tellerView.getjProgressBar_TellerTransfer().setBorder(BorderFactory.createTitledBorder("Processing..."));
                    tellerView.getjProgressBar_TellerTransfer().setIndeterminate(true);
                    tellerView.getjProgressBar_TellerTransfer().setVisible(true);
                }

                SwingWorker sw = new SwingWorker() 
                {                
                    @Override
                    protected Object doInBackground() throws Exception 
                    {
                        if(success)
                        {
                            ArrayList<Object> saveList = new ArrayList<>();
                            saveList.add(fromAccount);
                            saveList.add(toAccount);
                            
                            for(Transaction tran : fromAccount.getTransactions())
                            {
                                saveList.add(tran);
                            }
                            
                            for(Transaction tran : toAccount.getTransactions())
                            {
                                saveList.add(tran);
                            }
                            
                            try 
                            {
                                databaseUser.saveMany(saveList);
                                databaseUser.load();
                            } 
                            catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) 
                            {
                                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        return null;
                    }

                    @Override
                    public void done()
                    {
                        tellerView.getjProgressBar_TellerTransfer().setVisible(false);
                        if(success)
                        {
                            JOptionPane.showMessageDialog(null, "Transfer successful!");
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Transfer failure! Insufficient funds.");
                        }
                        tellerView.populateTransfer();
                    }
                };
                sw.execute();
            }
            
        }
    }
    
    class TellerOKListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            tellerView.setReferencedCust(null);
            tellerView.disableComponents();
            
            tellerView.getjProgressBar_TellerWD().setBorder(BorderFactory.createTitledBorder("Loading..."));
            tellerView.getjProgressBar_TellerWD().setIndeterminate(true);
            tellerView.getjProgressBar_TellerWD().setVisible(true);
            
            int userID = tellerView.getUserID();
                
            SwingWorker sw = new SwingWorker() 
            {                
                ArrayList<User> loadedUsers = new ArrayList<>();
                ArrayList<BankAccount> loadedAccounts = new ArrayList<>();
                
                @Override
                protected Object doInBackground() throws Exception 
                {
                    try 
                    {
                        loadedUsers = databaseUser.getUsers();
                        loadedAccounts = databaseUser.getAccounts();
                    } 
                    catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) 
                    {
                        Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }

                @Override
                public void done()
                {
                    if(userID != 0)
                    {
                        for(User user : loadedUsers)
                        {
                            if(user.getClass().equals(Customer.class) && user.getUserID() == userID)
                            {
                                tellerView.setReferencedCust((Customer)user);
                            }
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "No such customer!");
                    }
                    
                    if(tellerView.getReferencedCust() == null)
                    {
                        JOptionPane.showMessageDialog(null, "No such customer!");
                    }
                    else
                    {
                        tellerView.populateAccounts();
                    }
                    
                    tellerView.getjProgressBar_TellerWD().setVisible(false);
                }
            };
            sw.execute();
        }
    }
    
    class TellerOKListener2 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            tellerView.setReferencedCust(null);
            tellerView.resetTransfer();
            
            tellerView.getjProgressBar_TellerTransfer().setBorder(BorderFactory.createTitledBorder("Loading..."));
            tellerView.getjProgressBar_TellerTransfer().setIndeterminate(true);
            tellerView.getjProgressBar_TellerTransfer().setVisible(true);
            
            int userID = tellerView.getUserID2();
                
            SwingWorker sw = new SwingWorker() 
            {                
                ArrayList<User> loadedUsers = new ArrayList<>();
                ArrayList<BankAccount> loadedAccounts = new ArrayList<>();
                
                @Override
                protected Object doInBackground() throws Exception 
                {
                    try 
                    {
                        loadedUsers = databaseUser.getUsers();
                        loadedAccounts = databaseUser.getAccounts();
                    } 
                    catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) 
                    {
                        Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }

                @Override
                public void done()
                {
                    if(userID != 0)
                    {
                        for(User user : loadedUsers)
                        {
                            if(user.getClass().equals(Customer.class) && user.getUserID() == userID)
                            {
                                tellerView.setReferencedCust((Customer)user);
                            }
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "No such customer!");
                    }
                    
                    if(tellerView.getReferencedCust() == null)
                    {
                        JOptionPane.showMessageDialog(null, "No such customer!");
                    }
                    else
                    {
                        tellerView.populateTransfer();
                    }
                    
                    tellerView.getjProgressBar_TellerTransfer().setVisible(false);
                }
            };
            sw.execute();
        }
    }
    
    class TellerWithdrawalListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            double amount = tellerView.getWDAmount();

            BankAccount account = tellerView.getWDAccount();
            
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to withdraw $" + (new DecimalFormat("#0.00")).format(amount) + " from " + account.getReadableAccountType() + "?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                boolean success = ((Teller)vUser).recordWithdrawal(tellerView.getReferencedCust(), account.getAccountID(), amount);
                
                if(success)
                {
                    tellerView.getjProgressBar_TellerWD().setBorder(BorderFactory.createTitledBorder("Processing..."));
                    tellerView.getjProgressBar_TellerWD().setIndeterminate(true);
                    tellerView.getjProgressBar_TellerWD().setVisible(true);
                }

                SwingWorker sw = new SwingWorker() 
                {                
                    @Override
                    protected Object doInBackground() throws Exception 
                    {
                        if(success)
                        {
                            ArrayList<Object> saveList = new ArrayList<>(tellerView.getReferencedCust().findBankAccount(account.getAccountID()).getTransactions());
                            saveList.add(account);
                            
                            try 
                            {
                                databaseUser.saveMany(saveList);
                                databaseUser.load();
                            } 
                            catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) 
                            {
                                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        return null;
                    }

                    @Override
                    public void done()
                    {
                        tellerView.getjProgressBar_TellerWD().setVisible(false);
                        if(success)
                        {
                            JOptionPane.showMessageDialog(null, "Withdrawal successful!");
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Withdrawal failure! Insufficient funds.");
                        }
                        tellerView.populateAccounts();
                    }
                };
                sw.execute();
            }
            
        }
    }

    class TellerDepositListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            double amount = tellerView.getWDAmount();

            BankAccount account = tellerView.getWDAccount();
            
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to deposit $" + (new DecimalFormat("#0.00")).format(amount) + " into " + account.getReadableAccountType() + "?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                ((Teller)vUser).recordDeposit(tellerView.getReferencedCust(), account.getAccountID(), amount, tellerView.getDepositType());
                
                tellerView.getjProgressBar_TellerWD().setBorder(BorderFactory.createTitledBorder("Processing..."));
                tellerView.getjProgressBar_TellerWD().setIndeterminate(true);
                tellerView.getjProgressBar_TellerWD().setVisible(true);

                SwingWorker sw = new SwingWorker() 
                {                
                    @Override
                    protected Object doInBackground() throws Exception 
                    {
                        ArrayList<Object> saveList = new ArrayList<>(tellerView.getReferencedCust().findBankAccount(account.getAccountID()).getTransactions());
                        saveList.add(account);

                        try 
                        {
                            databaseUser.saveMany(saveList);
                            databaseUser.load();
                        } 
                        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) 
                        {
                            Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        return null;
                    }

                    @Override
                    public void done()
                    {
                        tellerView.getjProgressBar_TellerWD().setVisible(false);
                        JOptionPane.showMessageDialog(null, "Deposit successful!");
                        tellerView.populateAccounts();
                    }
                };
                sw.execute();
            }
            
        }
    }
    
    class CustomerReportsListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            javax.swing.JTextArea reportArea = customerView.getjTextArea_ReportArea();
            javax.swing.JScrollPane scrollPane = customerView.getjScrollPane3();
            String selected = customerView.getSelectedReport();
            
            if(!selected.matches("Choose a report to view...") && !selected.matches(""))
            {
                String report = "";                
                
                if(selected.matches("Bills"))
                {
                    report = ((Customer)vUser).generateBillReports();
                }                
                else
                {
                    int accountNum = Integer.parseInt(selected.split("#")[1]);
                    report = ((Customer)vUser).findBankAccount(accountNum).generateAccountReport();
                }
                
                reportArea.setText(report);
                scrollPane.setVisible(true);
            }
            else
            {
                scrollPane.setVisible(false);
            }
            
            customerView.repaint();
            customerView.revalidate();
        }
    }
    
    class CustomerPayListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            Bill selectedBill = customerView.getSelectedBill();
            
            double amount = customerView.getBillPayAmount();

            BankAccount account = customerView.getBillPayFromAccount();
            
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to pay $" + (new DecimalFormat("#0.00")).format(amount) + " toward the " + selectedBill.getAccountName() + "\nbill with ID #" + selectedBill.getUniqueID() + " from " + account.getReadableAccountType() + "?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                boolean success = ((Customer)vUser).payBill(selectedBill, account, amount);
                
                if(success)
                {
                    customerView.getjProgressBar_BillPay().setBorder(BorderFactory.createTitledBorder("Processing..."));
                    customerView.getjProgressBar_BillPay().setIndeterminate(true);
                    customerView.getjProgressBar_BillPay().setVisible(true);
                }

                SwingWorker sw = new SwingWorker() 
                {                
                    @Override
                    protected Object doInBackground() throws Exception 
                    {
                        if(success)
                        {
                            ArrayList<Object> saveList = new ArrayList<>();
                            saveList.add(account);
                            
                            saveList.add(new Transaction(java.time.LocalDate.now(), "Withdrawal", selectedBill.getAccountName() + " Bill with ID#" + selectedBill.getUniqueID(), amount, account.getBalance(), ((Customer)vUser).getUserID(), account.getAccountID(), 0));
                            
                            if(selectedBill.getTotal() > 0)
                            {
                                saveList.add(selectedBill);
                            }
                            
                            try 
                            {
                                databaseUser.saveMany(saveList);
                                
                                if(selectedBill.getTotal() == 0)
                                {
                                    databaseUser.deleteOne(selectedBill);
                                }
                                
                                databaseUser.load();
                                vUser = databaseUser.getUser(vUser.getUserID());
                            } 
                            catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex) 
                            {
                                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                        return null;
                    }

                    @Override
                    public void done()
                    {
                        customerView.getjProgressBar_BillPay().setVisible(false);
                        if(success)
                        {
                            JOptionPane.showMessageDialog(null, "Bill pay successful!");
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Bill pay failure! Insufficient funds or overpayment.");
                        }
                        customerView.populate((Customer)vUser);
                    }
                };
                sw.execute();
            }
            
            customerView.repaint();
            customerView.revalidate();
        }
    }
    
    class LogoutListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure you want to logout?","Question",JOptionPane.OK_CANCEL_OPTION);
            if(dialogResult == JOptionPane.OK_OPTION)
            {
                logOut();
            }
        }
    }
    
    public void logOut()
    {
        customerView.getContentPane().invalidate();
        customerView.getContentPane().validate();
        customerView.getContentPane().repaint();
        
        customerView.resetFocus();
        tellerView.resetFocus();
        
        customerView.setVisible(false);
	tellerView.setVisible(false);
	adminView.setVisible(false);
        
        adminView.removeButtonListeners();
        customerView.removeButtonListeners();
        tellerView.removeButtonListeners();
        
        vUser = null;
        
        loginView.clearFields();
        loginView.getUsernameTextField().requestFocusInWindow();
	loginView.setVisible(true);
    }
    
    class WindowListener extends WindowAdapter
    {
        @Override
        public void windowClosing(WindowEvent e)
        {
            databaseUser.closeConection();
            e.getWindow().dispose();
        }
    }
    
    public String getReadableType(java.lang.Class cl)
    {
        return splitCamelCase(cl.toString().substring(cl.toString().lastIndexOf(".") + 1));
    }
    
    private String splitCamelCase(String s)
    {
        return s.replaceAll(String.format("%s|%s|%s",
                               "(?<=[A-Z])(?=[A-Z][a-z])",
                               "(?<=[^A-Z])(?=[A-Z])",
                               "(?<=[A-Za-z])(?=[^A-Za-z])"
                            ),
                            " "
                         );
    }
    
    public void BillGenerator(int numToGen) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        ArrayList<Bill> generatedBills = new ArrayList<>();
        ArrayList<Customer> customers = new ArrayList<>();
        
        for(User user : databaseUser.getUsers())
        {
            if(Customer.class.isAssignableFrom(user.getClass()))
            {
                if(user.getUserID() > 10)
                {
                    customers.add((Customer)user);
                }
            }
        }
        
        for(int i=0; i<numToGen; i++)
        {
            Customer cust = customers.get(java.util.concurrent.ThreadLocalRandom.current().nextInt(0, customers.size()));
            ArrayList<Object> choices = new ArrayList<>();
            
            for(BillPayAccount bpa : cust.bp_accounts)
            {
                choices.add(bpa);
            }

            if(cust.hasAccount(Model.DataClasses.HomeMortgageAccount.class))
            {
                choices.add(cust.getHomeMortgage());
            }
            
            if(cust.hasAccount(Model.DataClasses.CreditCardAccount.class))
            {
                choices.add(cust.getCreditCard());
            }
            
            Object choice = choices.get(java.util.concurrent.ThreadLocalRandom.current().nextInt(0, choices.size()));
            Bill billToAdd = null;
            int total = java.util.concurrent.ThreadLocalRandom.current().nextInt(50, 300);
            
            if(BillPayAccount.class.isAssignableFrom(choice.getClass()))
            {
                BillPayAccount currBPA = (BillPayAccount)choice;
                billToAdd = new Bill("Fictionville", currBPA.getAccountName(), 0, currBPA.getAccountNum(), currBPA.getUserID(), total, LocalDate.of(2016, 12, 2));
            }
            else if(Model.DataClasses.HomeMortgageAccount.class.isAssignableFrom(choice.getClass()))
            {
                Model.DataClasses.HomeMortgageAccount currHMA = (Model.DataClasses.HomeMortgageAccount)choice;
                billToAdd = new Bill("Fictionville", "Home Mortgage Account", 0, currHMA.getAccountID(), currHMA.getUserID(), total, LocalDate.of(2016, 12, 2));
            }
            else if(Model.DataClasses.CreditCardAccount.class.isAssignableFrom(choice.getClass()))
            {
                Model.DataClasses.CreditCardAccount currCCA = (Model.DataClasses.CreditCardAccount)choice;
                billToAdd = new Bill("Fictionville", "Credit Card Account", 0, currCCA.getAccountID(), currCCA.getUserID(), total, LocalDate.of(2016, 12, 2));
            }
            
            if (billToAdd != null)
            {
                generatedBills.add(billToAdd);
                billToAdd.print();
            }
            
        }
            
        databaseUser.saveMany(generatedBills);
    }
}
