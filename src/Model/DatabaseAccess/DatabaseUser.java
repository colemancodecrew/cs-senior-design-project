package Model.DatabaseAccess;

import Model.DataClasses.Admin;
import Model.DataClasses.BankAccount;
import Model.DataClasses.BankAccountFactory;
import Model.DataClasses.Bill;
import Model.DataClasses.BillPayAccount;
import Model.DataClasses.CheckingAccount;
import Model.DataClasses.CreditCardAccount;
import Model.DataClasses.Customer;
import Model.DataClasses.HomeMortgageAccount;
import Model.DataClasses.MoneyMarketAccount;
import Model.DataClasses.SavingsAccount;
import Model.DataClasses.Teller;
import Model.DataClasses.Transaction;
import Model.DataClasses.User;
import Model.DataClasses.UserFactory;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Manish
 * @author Rob Vary
 */
public class DatabaseUser 
{   
    Connection con;
    private final ArrayList<User> users;
    private final ArrayList<BankAccount> accounts;
    private final ArrayList<BillPayAccount> bp_accounts;
    private final ArrayList<Bill> bills;
    private final ArrayList<Transaction> transactions;
    DatabaseConnection dbConnection;
    private final BankAccountFactory baf;
    private final UserFactory uaf;
    
    static private DatabaseUser singletonDatabaseUser;
    
    /**
     * singleton
     */
    private DatabaseUser() 
    {
        users = new ArrayList<>();
        accounts = new ArrayList<>();
        bp_accounts = new ArrayList<>();
        bills = new ArrayList<>();
        transactions = new ArrayList<>();
        dbConnection = new DatabaseConnection();
        
        try 
        {
            con = dbConnection.connect();
        } 
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) 
        {
            Logger.getLogger(DatabaseUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(con);
        
        baf = BankAccountFactory.getBankAccountFactory();
        uaf = UserFactory.getUserFactory();
    }
    
    public static DatabaseUser getDatabaseUser()
    {
        if (singletonDatabaseUser == null)
        {
            singletonDatabaseUser = new DatabaseUser();
        } 
        return singletonDatabaseUser;
    }
    
    /**
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void load() throws 
            SQLException, ClassNotFoundException, 
            InstantiationException, IllegalAccessException 
    {
        users.clear();
        accounts.clear();
        bp_accounts.clear();
        bills.clear();
        transactions.clear();
        int userID;
        String userName;
        String userPassword;
        String userType;
        String lastName;
        String firstName;
        String address;
        String city;
        
        Statement stmt = con.createStatement();        
        String sql = "SELECT UserName, AES_DECRYPT(UserPass, 'coleman') AS UserPass, UserType, UserID, LastName,"
                   + " FirstName, Address, City FROM Users ORDER BY UserID";
        try 
        {
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                userID = rs.getInt("UserID");
                userName = rs.getString("UserName");
                userPassword = rs.getString("UserPass");
                userType = rs.getString("UserType");
                lastName = rs.getString("LastName");
                firstName = rs.getString("FirstName");
                address = rs.getString("Address");
                city = rs.getString("City");
                
                switch(userType)
                {
                    case "Administrator":
                    {
                        users.add(uaf.createUser(Admin.class, userID, userName, userPassword, userType, lastName, firstName, address, city));
                    }
                    break;
                    
                    case "Teller":
                    {
                        users.add(uaf.createUser(Teller.class, userID, userName, userPassword, userType, lastName, firstName, address, city));
                    }
                    break;
                    
                    case "Customer":
                    {
                        users.add(uaf.createUser(Customer.class, userID, userName, userPassword, userType, lastName, firstName, address, city));
                    }
                    break;
                }
            }
        }
        catch (SQLException e) 
        {
            System.out.println(e.getMessage());
        }
        
        sql = "SELECT userID, accountID, accountType, balance, interest, fee_date, last_date_pay FROM Accounts ORDER BY accountID";
        
        try 
        {
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                switch(rs.getString("accountType"))
                {
                    case "Checking":
                    {
                        accounts.add(baf.createBankAccount(CheckingAccount.class, rs.getDouble("balance"), (rs.getString("interest") == null)? 0 : rs.getDouble("interest"), rs.getInt("userID"), rs.getInt("accountID"), convertToEntityAttribute(rs.getDate("fee_date")), convertToEntityAttribute(rs.getDate("last_date_pay"))));
                    }
                    break;
                    
                    case "Savings":
                    {
                        accounts.add(baf.createBankAccount(SavingsAccount.class, rs.getDouble("balance"), (rs.getString("interest") == null)? 0 : rs.getDouble("interest"), rs.getInt("userID"), rs.getInt("accountID"), convertToEntityAttribute(rs.getDate("fee_date")), convertToEntityAttribute(rs.getDate("last_date_pay"))));
                    }
                    break;
                    
                    case "Credit":
                    {
                        accounts.add(baf.createBankAccount(CreditCardAccount.class, rs.getDouble("balance"), (rs.getString("interest") == null)? 0 : rs.getDouble("interest"), rs.getInt("userID"), rs.getInt("accountID"), convertToEntityAttribute(rs.getDate("fee_date")), convertToEntityAttribute(rs.getDate("last_date_pay"))));
                    }
                    break;
                    
                    case "HomeMortgage":
                    {
                        accounts.add(baf.createBankAccount(HomeMortgageAccount.class, rs.getDouble("balance"), (rs.getString("interest") == null)? 0 : rs.getDouble("interest"), rs.getInt("userID"), rs.getInt("accountID"), convertToEntityAttribute(rs.getDate("fee_date")), convertToEntityAttribute(rs.getDate("last_date_pay"))));
                    }
                    break;
                    
                    case "MoneyMarket":
                    {
                        accounts.add(baf.createBankAccount(MoneyMarketAccount.class, rs.getDouble("balance"), (rs.getString("interest") == null)? 0 : rs.getDouble("interest"), rs.getInt("userID"), rs.getInt("accountID"), convertToEntityAttribute(rs.getDate("fee_date")), convertToEntityAttribute(rs.getDate("last_date_pay"))));
                    }
                    break;
                }
            }
        }
        catch (SQLException e) 
        {
            System.out.println(e.getMessage());
        }
        
        sql = "SELECT accountName, address, accountNum, userID, uniqueID FROM BillPayAccounts ORDER BY accountNum";
        
        try
        {
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                bp_accounts.add(new BillPayAccount(rs.getString("accountName"), rs.getString("address"), rs.getInt("accountNum"), rs.getInt("userID"), rs.getInt("uniqueID")));
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
        
        sql = "SELECT accountName, address, accountNum, userID, uniqueID, total, due_date FROM Bills ORDER BY uniqueID";
        
        try
        {
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                bills.add(new Bill(rs.getString("address"), rs.getString("accountName"), rs.getInt("uniqueID"), rs.getInt("accountNum"), rs.getInt("UserID"), rs.getDouble("total"), convertToEntityAttribute(rs.getDate("due_date"))));
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
        
        sql = "SELECT transac_date, transac_type, description, amount, balance, userID, accountID, uniqueID FROM Transactions ORDER BY uniqueID";
        
        try
        {
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next())
            {
                transactions.add(new Transaction(convertToEntityAttribute(rs.getDate("transac_date")), rs.getString("transac_type"), rs.getString("description"), rs.getDouble("amount"), rs.getDouble("balance"), rs.getInt("userID"), rs.getInt("accountID"), rs.getInt("uniqueID")));
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
        
        for(BankAccount acct : accounts)
        {
            for(Transaction trans : transactions)
            {
                if(trans.getAccountID() == acct.getAccountID())
                {
                    acct.addTransaction(trans);
                }
            }
        }
        
        for (User user : users)
        {
            if(user.getClass().equals(Customer.class))
            {            
                Customer cust = (Customer)user;
                int id = cust.getUserID();

                for(BankAccount acct : accounts)
                {
                    if(acct.getUserID() == id)
                    {
                        cust.addAccount(acct);
                    }
                }

                for(BillPayAccount acct : bp_accounts)
                {
                    if(acct.getUserID() == id)
                    {
                        cust.addBillPay(acct);
                    }
                }
                
                for(Bill bill : bills)
                {
                    if(bill.getUserID() == id)
                    {
                        cust.addBill(bill);
                    }
                }
            }
        }
    }
    
    public boolean saveMany(ArrayList<?> list) throws 
            ClassNotFoundException, InstantiationException, 
            IllegalAccessException, SQLException
    {
        load();
        
        for (Object item : list)
        {
            int ID = (User.class.isAssignableFrom(item.getClass())) ? ((User)item).getUserID() : (BankAccount.class.isAssignableFrom(item.getClass())) ? ((BankAccount)item).getAccountID() : (BillPayAccount.class.isAssignableFrom(item.getClass())) ? ((BillPayAccount)item).getUniqueID() : (Bill.class.isAssignableFrom(item.getClass())) ? ((Bill)item).getUniqueID() : ((Transaction)item).getUniqueID();
            String table = (User.class.isAssignableFrom(item.getClass())) ? "Users" : (BankAccount.class.isAssignableFrom(item.getClass())) ? "Accounts" : (BillPayAccount.class.isAssignableFrom(item.getClass())) ? "BillPayAccounts" : (Bill.class.isAssignableFrom(item.getClass())) ? "Bills" : "Transactions";
            
            if(inList(item, (User.class.isAssignableFrom(item.getClass())) ? users : (BankAccount.class.isAssignableFrom(item.getClass())) ? accounts : (BillPayAccount.class.isAssignableFrom(item.getClass())) ? bp_accounts : (Bill.class.isAssignableFrom(item.getClass())) ? bills : transactions))
            {
                String values = (table.equals("Users")) ?
                                "UserName=\"" + ((User)item).getUserName() + "\"" +
                                ", UserPass=AES_ENCRYPT('" + ((User)item).getUserPassword() + "', 'coleman')" +
                                ", LastName=\"" + ((User)item).getLastName() + "\"" +
                                ", FirstName=\"" + ((User)item).getFirstName() + "\"" +
                                ", Address=\"" + ((User)item).getAddress() + "\"" +
                                ", City=\"" + ((User)item).getCity() + "\"" :
                                (table.equals("Accounts")) ?
                                "balance=\"" + ((BankAccount)item).getBalance() + "\"" +
                                ", interest=\"" + ((BankAccount)item).getPIR() + "\"" + 
                                ", fee_date=\"" + convertToDatabaseColumn(((BankAccount)item).getLastDate()) + "\"" + 
                                ", last_date_pay=\"" + convertToDatabaseColumn(((BankAccount)item).getLastDatePayReceived()) + "\"": 
                                (table.equals("BillPayAccounts")) ?
                                "accountName=\"" + ((BillPayAccount)item).getAccountName() + "\"" + 
                                ", accountNum=\"" + ((BillPayAccount)item).getAccountNum() + "\"" +
                                ", address=\"" + ((BillPayAccount)item).getAddress() + "\"" + 
                                ", userID=\"" + ((BillPayAccount)item).getUserID() + "\"" : 
                                (table.equals("Bills")) ? 
                                "address=\"" + ((Bill)item).getAddress() + "\"" + 
                                ", accountName=\"" + ((Bill)item).getAccountName() + "\"" + 
                                ", accountNum=\"" + Integer.toString(((Bill)item).getAccountNum()) + "\"" + 
                                ", userID=\"" + Integer.toString(((Bill)item).getUserID()) + "\"" + 
                                ", total=\"" + Double.toString(((Bill)item).getTotal()) + "\"" + 
                                ", due_date=\"" + convertToDatabaseColumn(((Bill)item).getDue_date()) + "\"" : 
                                "transac_date=\"" + convertToDatabaseColumn(((Transaction)item).getTransac_date()) + "\"" +
                                ", transac_type=\"" + ((Transaction)item).getTransac_type() + "\"" +
                                ", description=\"" + ((Transaction)item).getDescription() + "\"" +
                                ", amount=\"" + Double.toString(((Transaction)item).getAmount()) + "\"" +
                                ", balance=\"" + Double.toString(((Transaction)item).getBalance()) + "\"" +
                                ", userID=\"" + Integer.toString(((Transaction)item).getUserID()) + "\"" +
                                ", accountID=\"" + Integer.toString(((Transaction)item).getAccountID()) + "\"";

                String sqlKey = (table.equals("Users")) ? "UserID" : (table.equals("Accounts")) ? "accountID" : "uniqueID";

                String sql = "UPDATE " + table + " SET " + values + " WHERE " + sqlKey + "=" + Integer.toString(ID);

                if(!table.equals("Transactions"))
                {
                    Statement stmt = con.createStatement();
                    stmt.executeUpdate(sql);
                }
            }
            else
            {
                User userItem = (table.equals("Users")) ? (User)item : null;
                BankAccount acctItem = (table.equals("Accounts")) ? (BankAccount)item : null;
                BillPayAccount bpItem = (table.equals("BillPayAccounts")) ? (BillPayAccount)item : null;
                Bill billItem = (table.equals("Bills")) ? (Bill)item : null;
                Transaction tranItem = (table.equals("Transactions")) ? (Transaction)item : null;
                
                String cols = (table.equals("Users")) ?
                                "UserType, UserName, UserPass, LastName, FirstName, Address, City" : (table.equals("Accounts")) ?
                                "userID, accountType, balance, fee_date, last_date_pay" : (table.equals("BillPayAccounts")) ? 
                                "accountName, address, accountNum, userID" : (table.equals("Bills")) ? 
                                "accountName, address, accountNum, userID, total, due_date" : 
                                "transac_date, transac_type, description, amount, balance, userID, accountID";
                String values = (table.equals("Users")) ?
                                "'" + userItem.getUserType() + "', " +
                                "'" + userItem.getUserName() + "', " +
                                "AES_ENCRYPT('" + userItem.getUserPassword() + "', 'coleman'), " +
                                "'" + userItem.getLastName() + "', " +
                                "'" + userItem.getFirstName() + "', " +
                                "'" + userItem.getAddress() + "', " +
                                "'" + userItem.getCity() + "'" :
                                (table.equals("Accounts")) ?
                                "'" + acctItem.getUserID() + "', " +
                                "'" + acctItem.getReadableAccountTypeForSQL() + "', " +
                                "'" + acctItem.getBalance() + "', " + 
                                "'" + convertToDatabaseColumn(LocalDate.now()) + "', " + 
                                "'" + convertToDatabaseColumn(LocalDate.now()) + "'" :
                                (table.equals("BillPayAccounts")) ? 
                                "'" + bpItem.getAccountName() + "', " +
                                "'" + bpItem.getAddress() + "', " +
                                "'" + bpItem.getAccountNum() + "', " +
                                "'" + bpItem.getUserID() + "'" :
                                (table.equals("Bills")) ? 
                                "'" + billItem.getAccountName() + "', " +
                                "'" + billItem.getAddress() + "', " +
                                "'" + billItem.getAccountNum() + "', " +
                                "'" + billItem.getUserID() + "', " +
                                "'" + billItem.getTotal() + "', " +
                                "'" + convertToDatabaseColumn(billItem.getDue_date()) + "'" :
                                "'" + convertToDatabaseColumn(tranItem.getTransac_date()) + "', " + 
                                "'" + tranItem.getTransac_type() + "', " +
                                "'" + tranItem.getDescription() + "', " +
                                "'" + tranItem.getAmount() + "', " +
                                "'" + tranItem.getBalance() + "', " +
                                "'" + tranItem.getUserID() + "', " +
                                "'" + tranItem.getAccountID() + "'";
                
                if(table.equals("BankAccount"))
                {
                    cols += (item.getClass().equals(CheckingAccount.class)) ? "" : ", interest";
                    values += (item.getClass().equals(CheckingAccount.class)) ? "" : ", " + acctItem.getPIR();
                }
                
                String sql = "INSERT INTO " + table + " (" + cols + ")\n" +
                             "VALUES (" + values + ");";
                
                Statement stmt = con.createStatement();
                stmt.executeUpdate(sql);
            }
        }
        
        return true;
    }
    
    public boolean saveOne(Object obj) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException
    {
        ArrayList<Object> list = new ArrayList<>();
        list.add(obj);
        return saveMany(list);
    }
    
    public boolean deleteMany(ArrayList<?> list) throws 
            ClassNotFoundException, InstantiationException, 
            IllegalAccessException, SQLException
    {
        load();
        
        ArrayList<Object> notInDB = new ArrayList<>();
        
        for (Object item : list)
        {
            int ID = (User.class.isAssignableFrom(item.getClass())) ? ((User)item).getUserID() : (BankAccount.class.isAssignableFrom(item.getClass())) ? ((BankAccount)item).getAccountID() : (BillPayAccount.class.isAssignableFrom(item.getClass())) ? ((BillPayAccount)item).getUniqueID() : (Bill.class.isAssignableFrom(item.getClass())) ? ((Bill)item).getUniqueID() : ((Transaction)item).getUniqueID();
            String table = (User.class.isAssignableFrom(item.getClass())) ? "Users" : (BankAccount.class.isAssignableFrom(item.getClass())) ? "Accounts" : (BillPayAccount.class.isAssignableFrom(item.getClass())) ? "BillPayAccounts" : (Bill.class.isAssignableFrom(item.getClass())) ? "Bills" : "Transactions";
            
            if(inList(item, (User.class.isAssignableFrom(item.getClass())) ? users : (BankAccount.class.isAssignableFrom(item.getClass())) ? accounts : (BillPayAccount.class.isAssignableFrom(item.getClass())) ? bp_accounts : (Bill.class.isAssignableFrom(item.getClass())) ? bills : transactions))
            {
                String sqlKey = (table.equals("Users")) ? "UserID" : (table.equals("Accounts")) ? "accountID" : "uniqueID";
                
                String sql = "DELETE FROM " + table + " WHERE " + sqlKey + "=" + ID;
                
                Statement stmt = con.createStatement();
                stmt.executeUpdate(sql);
            }
            else
            {
                notInDB.add(item);
            }
        }
        
        if(!notInDB.isEmpty())
        {
            System.out.println("The following entries were not found in the database:");
            
            for (Object obj : notInDB)
            {
                if (User.class.isAssignableFrom(obj.getClass()))
                {
                    ((User)obj).print();
                }
                else if (BankAccount.class.isAssignableFrom(obj.getClass()))
                {
                    ((BankAccount)obj).print();
                }
                else if (BillPayAccount.class.isAssignableFrom(obj.getClass()))
                {
                    ((BillPayAccount)obj).print();
                }
                else if (Bill.class.isAssignableFrom(obj.getClass()))
                {
                    ((Bill)obj).print();
                }
                else
                {
                    System.out.println(((Transaction)obj).toString());
                }
            }
        }
        
        return true;
    }
    
    public boolean deleteOne(Object obj) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException
    {
        ArrayList<Object> list = new ArrayList<>();
        list.add(obj);
        return deleteMany(list);
    }
    
    /**
     *
     * @param vUserId
     * @param vUserPassword
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public User validateUser(String vUserId, String vUserPassword) 
            throws SQLException, ClassNotFoundException, 
            InstantiationException, IllegalAccessException 
    {
        for (User ul : users) 
        {
            if (ul.getUserName().equals(vUserId)) 
            {
                if (ul.getUserPassword().equals(vUserPassword)) 
                {
                    return ul;
                } 
            }
        }
        return null;
    }
    
    /**
    *   basic search method to see if the needle object is in the haystack list
    *   works for User, BankAccount, BillPayAccount, Bill, and Transaction types
    *   @param needle
    *   @param haystack
    */
    private boolean inList(Object needle, ArrayList<?> haystack)
    {
        int key = (User.class.isAssignableFrom(needle.getClass())) ? ((User)needle).getUserID() : (BankAccount.class.isAssignableFrom(needle.getClass())) ? ((BankAccount)needle).getAccountID() : (BillPayAccount.class.isAssignableFrom(needle.getClass())) ? ((BillPayAccount)needle).getUniqueID() : (Bill.class.isAssignableFrom(needle.getClass())) ? ((Bill)needle).getUniqueID() : ((Transaction)needle).getUniqueID();
        
        for (Object item : haystack)
        {
            int itemKey = (User.class.isAssignableFrom(item.getClass())) ? ((User)item).getUserID() : (BankAccount.class.isAssignableFrom(item.getClass())) ? ((BankAccount)item).getAccountID() : (BillPayAccount.class.isAssignableFrom(item.getClass())) ? ((BillPayAccount)item).getUniqueID() : (Bill.class.isAssignableFrom(item.getClass())) ? ((Bill)item).getUniqueID() : ((Transaction)item).getUniqueID();
            
            if (itemKey == key)
            {
                return true;
            }
        }
        
        return false;
    }
    
    public ArrayList<User> getUsers() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        load();
        return users;
    }

    public ArrayList<BankAccount> getAccounts() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        load();
        return accounts;
    }
    
    public ArrayList<BillPayAccount> getBillPayAccounts() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException 
    {
        load();
        return bp_accounts;
    }
    
    public String getMySQLEntry(int id, String table, String column) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException
    {
        String sql = "SELECT " + column + " FROM " + table + " WHERE " + 
                     ((table.equals("Users")) ? "UserID" : 
                     (table.equals("Accounts")) ? "accountID" : "uniqueID") + "=" + Integer.toString(id);
        
        Statement stmt = con.createStatement();         
        ResultSet rs = stmt.executeQuery(sql);
        
        String entry = rs.getObject(column, String.class);
        
        return entry;
    }
    
    private java.sql.Date convertToDatabaseColumn(LocalDate entityValue) 
    {
        return (entityValue == null) ? null : java.sql.Date.valueOf(entityValue);
    }

    private LocalDate convertToEntityAttribute(java.sql.Date databaseValue) 
    {
        return (databaseValue == null) ? null : databaseValue.toLocalDate();
    }
    
    public void closeConection()
    {
        try 
        {
            con.close();
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(DatabaseUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Connection closed successfully");
    }
    
    public User getUser(int userID) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        for(User user : users)
        {
            if(user.getUserID() == userID)
            {
                return user;
            }
        }
        
        return null;
    }
}