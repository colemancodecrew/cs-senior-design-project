package Model.DatabaseAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Manish
 * @author Rob Vary
 */
public class DatabaseConnection 
{
   
    //added lib/mysql-connector-java-5.1.39-bin.jar for this
    public final static String dbURL = "com.mysql.jdbc.Driver";
    Connection conn = null;

    /**
     *
     * @return Connection Derby embedded database connection
     * @throws ClassNotFoundException Thrown in event of missing Derby class
     * @throws InstantiationException Thrown in event of inability to instantiate Derby class
     * @throws IllegalAccessException Thrown in event of user access rights issue
     */
    public Connection connect() throws ClassNotFoundException, 
            InstantiationException, IllegalAccessException 
    {        
        try 
        {
            Class.forName(dbURL).newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://www.db4free.net:3306/cs499bmudb", "varygoode", "coleman");
            System.out.println("DB Connection created successfully");
            return conn;
        } 
        catch(SQLException sqle) 
        {
            JOptionPane.showMessageDialog(null, sqle.toString());
            return null;
        }        
    }
    
    /**
     *
     * @throws SQLException Thrown in event of database close error.
     */
    public void close() throws SQLException 
    {
        if (conn != null) 
        {
            conn.close();
        }           
    }

}