/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DataClasses;

/**
 *
 * @author Mr J
 */
public class UserFactory 
{
    static private UserFactory singletonFactory;
    
    private UserFactory()
    {

    }
    
    public static UserFactory getUserFactory()
    {
        if (singletonFactory == null)
        {
            singletonFactory = new UserFactory();
        } 
        return singletonFactory;
    }
     /**
      * 
      * @param classArg
      * @param userID
      * @param userName
      * @param userPassword
      * @param userType
      * @param lastName
      * @param firstName
      * @param address
      * @param city
      * @return 
      * make sure return type is as expected;
      * default behavior returns null if no Class parameter is specified;
      */       
    public User createUser(Class<?> classArg, int userID, String userName, String userPassword, String userType, 
                String lastName, String firstName, String address, String city)
    {
        if (classArg.equals(Model.DataClasses.Customer.class))
        {
            return new Customer(userID, userName, userPassword, userType, 
               lastName, firstName, address, city);
        }
        else if (classArg.equals(Model.DataClasses.Teller.class))
        {
            return new Teller(userID, userName, userPassword, userType, 
                lastName, firstName, address, city);
        }
	else if (classArg.equals(Model.DataClasses.Admin.class))
        {
            return new Admin(userID, userName, userPassword, userType, 
                lastName, firstName, address, city);
        }
        else            
        {System.out.println("Unspecified user type");
        return null;
        }
    } 
    
    public void getMessage()
    {
        System.out.println("User created");
    }
}