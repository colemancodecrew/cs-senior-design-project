package Model.DataClasses;

import java.time.LocalDate;

/**
 * BankAccountFactory
 * @author Cozette Napoles
 */
public class BankAccountFactory {
    
    static private BankAccountFactory singletonFactory;
    
    private BankAccountFactory()
    {
        
    }
    
    public static BankAccountFactory getBankAccountFactory()
    {
        if (singletonFactory == null)
        {
            singletonFactory = new BankAccountFactory();
        } 
        return singletonFactory;
    }
    /**
    * 
    * @param classArg The type of account being created.
    * @param balance The starting balance of the account.
    * @param pir The periodic interest rate for the account (if applicable; 0 if not).
    * @param userID The corresponding user for this account.
    * @param accountID
    * @param lastDate Last date the interest was calculated
    * @param lastDatePayReceived Last date a payment was received for applicable accounts
    * @return BankAccount subclass
    * Returns null if incorrect, or no value, is provided.
    */                
    public BankAccount createBankAccount(Class<?> classArg, double balance, 
    double pir, int userID, int accountID, LocalDate lastDate, LocalDate lastDatePayReceived)
    {
        if (classArg.equals(Model.DataClasses.CheckingAccount.class))
        {
            return new CheckingAccount(balance, pir, userID, accountID, lastDate, 
                                                            lastDatePayReceived);
        }
        else if (classArg.equals(Model.DataClasses.SavingsAccount.class))
        {
            return new SavingsAccount(balance, pir, userID, accountID, lastDate,
                                                            lastDatePayReceived);
        }
        else if (classArg.equals(Model.DataClasses.CreditCardAccount.class))
        {
          return new CreditCardAccount(balance, pir, userID, accountID, lastDate,
                                                            lastDatePayReceived);
        }
        else if (classArg.equals(Model.DataClasses.MoneyMarketAccount.class))
        {
          return new MoneyMarketAccount(balance, pir, userID, accountID, lastDate,
                                                            lastDatePayReceived);
        }
        else if (classArg.equals(Model.DataClasses.HomeMortgageAccount.class))
        {
          return new HomeMortgageAccount(balance, pir, userID, accountID, lastDate,
                                                            lastDatePayReceived);
        }
        System.out.println("Unspecified user type");
        return null;
    } 
}