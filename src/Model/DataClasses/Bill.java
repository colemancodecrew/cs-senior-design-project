/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DataClasses;

import java.time.LocalDate;

/**
 * member variable ideas provided solely by MR. ZANE JOHNS
 * @author Rob
 */
public class Bill 
{
    private String address, accountName;
    private int uniqueID, accountNum, userID;
    private double total;
    private LocalDate due_date;
    
    public Bill(String address, String accountName, int uniqueID, int accountNum, int userID, double total, LocalDate due_date) 
    {
        this.address = address;
        this.accountName = accountName;
        this.uniqueID = uniqueID;
        this.accountNum = accountNum;
        this.userID = userID;
        this.total = total;
        this.due_date = due_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public int getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(int uniqueID) {
        this.uniqueID = uniqueID;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(int accountNum) {
        this.accountNum = accountNum;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public LocalDate getDue_date() {
        return due_date;
    }

    public void setDue_date(LocalDate due_date) {
        this.due_date = due_date;
    }
    
    public void print()
    {
        System.out.println("======================");
        System.out.println("Type: Bill");
        System.out.println("AccountName: " + accountName);
        System.out.println("AccountNum: " + accountNum);
        System.out.println("UserID: " + userID);
        System.out.println("Address: " + address);
        System.out.println("UniqueID: " + uniqueID);
        System.out.println("Total: " + total);
        System.out.println("Due Date: " + due_date.toString());
        System.out.println("======================");
    }
}
