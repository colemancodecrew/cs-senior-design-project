package Model.DataClasses;

import java.time.LocalDate;
import java.util.ArrayList;
/**
 * BankAccount Parent Class
 * @author Cozette Napoles
 */
public abstract class BankAccount
{   
    protected String statement;
    protected int userID, accountID;
    protected double balance;
    protected double pir; //Periodic Interest Rate
    protected boolean paymentReceived;
    protected double paymentAmount;
    protected LocalDate today;
    protected LocalDate lastDate;
    protected LocalDate lastDatePayReceived;
    protected ArrayList<Transaction> transactions;
    
    public BankAccount(int userID, int accountID, double balance, double pir,
                        LocalDate lastDate, LocalDate lastDatePayReceived) 
    {
        this.balance = balance;
        this.pir = pir;
        this.userID = userID;
        this.accountID = accountID;
        this.paymentAmount = 0.0;
        this.transactions = new ArrayList<>(); 
        this.lastDate = lastDate; //For calculating interest
        this.lastDatePayReceived = lastDatePayReceived; //For accounts with payments
        transactions = new ArrayList<>();
        statement = "";
    }
    
    /**
     * @param type The type of transaction (withdrawal, deposit, interest, etc).
     * @param who The entity that performed the transaction.
     * @param amount The amount being added or subtracted from the account.
     * @return balance
    */
    public double updateBalance(String type, String who, double amount)
    {
        //Check if we are receiving a payment for either of these account types.
        if (this.getAccountType().equals("HomeMortgageAccount") || 
                                    this.getAccountType().equals("CreditCardAccount"))
        {
            setPaymentReceived(amount);
        }
        
        balance = amount + balance;
        today = LocalDate.now();
        setTransaction(type, who, amount);
        
        return balance;
    }
    
    /*  calcInterest calculates the interest for an account
        based on the date. If it is a new day, the interest is
        calculated and added to the balance. If it is the same
        day, it is not updated. 
     */
    public void calcInterest()
    {
        //Create new Calendar object
        today = LocalDate.now();
        String typeInterest = "Interest";
        String whoInterest = "Hello World Bank";
        double amountInterest;
        int daysPassed;
        int yearsPassed = 0;
       
        if (today != lastDate)
        {
            if (today.getYear() != lastDate.getYear())
            {
                yearsPassed = today.getYear() - lastDate.getYear();
            }
            
            daysPassed = today.getDayOfYear() - lastDate.getDayOfYear();
            //Turn the PIR into a percentage and divide the PIR by a 30 day period
            amountInterest = ((yearsPassed * 365) + daysPassed) * (balance * ((pir/100)/30));
            //Round to two decimal places
            amountInterest = Math.round(amountInterest*100.0)/100.0;
            balance = amountInterest + balance;
            if (amountInterest > 0)
            {
                setTransaction(typeInterest, whoInterest, amountInterest);
            }
            setLastDate(today, lastDate); //Update the date
        }
    }
    
    /**
     * @param amount The amount that is updating the balance of the account.
     * @return paymentReceived Whether the minimum payment has been received.
     */
    public boolean setPaymentReceived(double amount)
    {
        today = LocalDate.now();
        
        //Don't reset the paymentReceived value if the user has already made 
        // their minimum payment for the month.
        if (today.getMonthValue() == lastDatePayReceived.getMonthValue())
        {
            if (paymentReceived)
            {
                return paymentReceived;
            }
        }
        
        //Calculate the minimum payment due (if it was not previously calculated.)
        if (this.getPaymentAmount() == 0.0)
        {
            this.setPaymentAmount();
        }
        
        //If the amount is a negative number, the Home Mortgage or Credit
        // Card accounts are receiving a payment.
        if (amount < 0)
        {
            //Will this satisfy the minimum payment due?
            if (amount <= this.getPaymentAmount())
            {
                paymentReceived = true;
            }
            else //Wasn't enough money to pay the minimum amount.
            {
                paymentReceived = false;
            }
        }
        
        //Update the value for the next time this method is called.
        setLastDatePayReceived(today, lastDatePayReceived);
        
        return paymentReceived;    
    }
    
    /* Checks if a payment has been received for certain accounts.
     * Returns the updated balance. 
     * */
    public void checkFeeReceived()
    {
        today = LocalDate.now();
        double monthsPassed;
        double feeAmount;
        String type = "Late Payment Fee";
        String lateFeeAccount;
        
        if (this.getAccountType().equals("HomeMortgageAccount"))
        {
            lateFeeAccount = "Home Mortgage";
        }
        else
        {
            lateFeeAccount = "Credit Card";
        }
       
        if (!paymentReceived && today.getMonthValue() > lastDatePayReceived.getMonthValue())
        {
            monthsPassed = today.getMonthValue() - lastDatePayReceived.getMonthValue();
            feeAmount = 25 * monthsPassed;
            //Round to two decimal places
            feeAmount = Math.round(feeAmount*100.0)/100.0;
            balance = balance + feeAmount;
            setTransaction(type, lateFeeAccount, feeAmount);
        }
    }
    
    /**
    * @return statement
    */
    public String getStatement() 
    {
        statement = "";
        
        for(Transaction tran : transactions)
        {
            statement += tran.toString();
        }
        
        return statement;
    }
        
    /**
    * @return balance
    */
    public double getBalance()
    {
        return balance;
    }
    
    /**
    * @param balance The balance of the account.
    */
    public void setBalance(double balance)
    {
        this.balance = balance;
    }
    
    /**
    * @param pir The periodic interest rate for the account.
    */
    public void setPIR(double pir)
    {
        this.pir = pir;
    }
    
    /**
    * @return pir
    */
    public double getPIR()
    {
        return pir;
    }
    
    public int getUserID() 
    {
        return userID;
    }

    public void setUserID(int userID) 
    {
        this.userID = userID;
    }
    
    public int getAccountID() 
    {
        return accountID;
    }

    public void setAccountID(int accountID) 
    {
        this.accountID = accountID;
    }
    
    /*
     * Calculates the minimum amount due each month for this account type.
    */
    protected void setPaymentAmount()
    {
        if (this.getAccountType().equals("HomeMortgageAccount"))
        {
            paymentAmount = balance * 0.01;
            paymentAmount = Math.round(paymentAmount*100.0)/100.0;   
        }
        else if (this.getAccountType().equals("CreditCardAccount"))
        {
            paymentAmount = balance * 0.02;
            paymentAmount = Math.round(paymentAmount*100.0)/100.0;
        }
        else
        {
            paymentAmount = 0.0;
        }
    }
    
    /**
     * @return The minimum paymentAmount 
     */
    public double getPaymentAmount()
    {
        return paymentAmount;
    }
    
    /* Update the LocalDate values for various calculations.*/
    protected void setLastDate(LocalDate today, LocalDate lastDate)
    {
        lastDate = today;
    }

    protected void setLastDatePayReceived(LocalDate today, 
                                            LocalDate lastDatePayReceived)
    {
        lastDatePayReceived = today;
    }
        
    public LocalDate getLastDate()
    {
        return lastDate;
    }
    
    public LocalDate getLastDatePayReceived()
    {
        return lastDatePayReceived;
    }
    
    public String getAccountType()
    {
//        String str = this.getClass().toString();
//        String[] parts = str.split(".");
//        return parts[parts.length - 1];
        return this.getClass().toString().substring(this.getClass().toString().lastIndexOf(".") + 1);
    }
    
    private void setTransaction(String type, String descript, double amount)
    {
        int uniqueID = 0;
        Transaction transaction = new Transaction(today, type, descript, amount, balance, userID, accountID, uniqueID);
        addTransaction(transaction);
    }
    
    public ArrayList<Transaction> getTransactions()
    {
        return transactions;
    }
    
    public void print()
    {
        System.out.println("======================");
        System.out.println("Type: " + getAccountType());
        System.out.println("AccountID: " + accountID);
        System.out.println("UserID: " + userID);
        System.out.println("Balance: " + balance);
        System.out.println("PIR: " + pir);
        System.out.println("======================");
    }
    
    public String getReadableAccountType()
    {
        return splitCamelCase(this.getClass().toString().substring(this.getClass().toString().lastIndexOf(".") + 1));
    }
    
    public String getReadableAccountTypeForSQL()
    {
        String temp = this.getClass().toString().substring(this.getClass().toString().lastIndexOf(".") + 1);
        temp = temp.substring(0, temp.indexOf("Account"));
        
        if(temp.equals("CreditCard"))
        {
            temp = "Credit";
        }
        
        return temp;
    }
    
    private String splitCamelCase(String s)
    {
        return s.replaceAll(String.format("%s|%s|%s",
                               "(?<=[A-Z])(?=[A-Z][a-z])",
                               "(?<=[^A-Z])(?=[A-Z])",
                               "(?<=[A-Za-z])(?=[^A-Za-z])"
                            ),
                            " "
                         );
    }  
    
    public String truncatedAcctNum()
    {
        String s = Integer.toString(accountID);
        return s.substring(0, Math.min(s.length(), 4));
    }
    
    public String generateAccountReport()
    {
        String R = "";
        R += "UserID: ";
        R += Integer.toString(getUserID()) + "\n";
            R += "AccountID: ";
        R += Integer.toString(getAccountID()) + "\n";
            R += "AccountType: ";
        R += getAccountType() + "\n";
            R += "Balance: ";
        R += Double.toString(getBalance()) + "\n";
            R += "Rate: ";
        R += Double.toString(getPIR()) + "\n";
        //for transactions
        R += "Statement:\n";
        R += getStatement() + "\n";
    return R;
    }
    
    public void addTransaction(Transaction transaction)
    {
        transactions.add(transaction);
    }
}