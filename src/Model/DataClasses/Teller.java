/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DataClasses;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Manish
 * @author Rob Vary
 */
public class Teller extends User
{
    public Teller(int userID, String userName, String userPassword, String userType, 
                String lastName, String firstName, String address, String city) 
    {
        super(userID, userName, userPassword, userType, 
                lastName, firstName, address, city);
    }
    
    
    public void recordDeposit(Customer theCustomer, int accountID, double Amount, String depositType)
    {
        theCustomer.findBankAccount(accountID).setBalance(theCustomer.findBankAccount(accountID).getBalance() + Amount);
        theCustomer.findBankAccount(accountID).addTransaction(new Transaction(java.time.LocalDate.now(), "Deposit", "Teller " + depositType + " Deposit", Amount, theCustomer.findBankAccount(accountID).getBalance(), theCustomer.getUserID(), accountID, 0));
    }
    
    
    public boolean recordWithdrawal(Customer theCustomer, int accountID, double Amount)
    {
        if(theCustomer.findBankAccount(accountID).getBalance() >= Amount)
        {
            theCustomer.findBankAccount(accountID).setBalance(theCustomer.findBankAccount(accountID).getBalance() - Amount);
            theCustomer.findBankAccount(accountID).addTransaction(new Transaction(java.time.LocalDate.now(), "Withdrawal", getRandomWithdrawalDescription(), Amount, theCustomer.findBankAccount(accountID).getBalance(), theCustomer.getUserID(), accountID, 0));
            return true;
        }
        
        return false;
    }
    
    private String getRandomWithdrawalDescription()
    {
        ArrayList<String> descriptions = new ArrayList<>(Arrays.asList("UAH Bookstore", "Taco Bell", "Best Buy", "UAH Dining", "Kroger", "Exxon", "McDonald's", "Bojangles", "Wal-Mart", "Publix", "PayPal", "Al's Toy Barn", "Kwik-E-Mart", "City Wok", "Krusty Krab", "Buy N Large", "Los Pollos Hermanos", "Weasleys' Wizard Wheezes", "ACME", "Big Kahuna Burger", "Amazon.com", "Zappos.com"));
        
        return descriptions.get(java.util.concurrent.ThreadLocalRandom.current().nextInt(0, descriptions.size()));
    }
}    