package Model.DataClasses;

import java.time.LocalDate;

/**
 * CheckingAccount subclass
 * @author Cozette Napoles
 */
public class CreditCardAccount extends BankAccount
{
    
    public CreditCardAccount(double balance, double pir, int userID, int accountID, 
                                LocalDate lastDate, LocalDate lastDatePayReceived) 
    {
           super(userID, accountID, balance, pir, lastDate, lastDatePayReceived);
    }
}
