/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DataClasses;

import java.text.DecimalFormat;
import java.time.LocalDate;

/**
 *
 * @author Rob
 */
public class Transaction 
{    
    private LocalDate transac_date;
    private String transac_type, description;
    private double amount, balance;
    private int userID, accountID, uniqueID;
    
    public Transaction(LocalDate transac_date, String transac_type, String description, Double amount, Double balance, int userID, int accountID, int uniqueID) {
        this.transac_date = transac_date;
        this.transac_type = transac_type;
        this.description = description;
        this.amount = amount;
        this.balance = balance;
        this.userID = userID;
        this.accountID = accountID;
        this.uniqueID = uniqueID;
    }

    public LocalDate getTransac_date() {
        return transac_date;
    }

    public void setTransac_date(LocalDate transac_date) {
        this.transac_date = transac_date;
    }

    public String getTransac_type() {
        return transac_type;
    }

    public void setTransac_type(String transac_type) {
        this.transac_type = transac_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(int uniqueID) {
        this.uniqueID = uniqueID;
    }
    
    public String toString()
    {
        return "================\n" + 
                "Date: " + transac_date.toString() + "\n" +
                "Transaction Type: " + transac_type + "\n" +
                "Description: " + description + "\n" +
                "Amount: $" + (new DecimalFormat("#0.00")).format(amount) + "\n" +
                "Balance After: $" + (new DecimalFormat("#0.00")).format(balance) + "\n";
    }
    
}
