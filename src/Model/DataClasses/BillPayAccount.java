/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DataClasses;

/**
 *
 * @author Rob
 */
public class BillPayAccount 
{
    private String accountName, address;
    private int accountNum, userID, uniqueID;
    
    public BillPayAccount(String accountName, String address, int accountNum, int userID, int uniqueID) 
    {
        this.accountName = accountName;
        this.address = address;
        this.accountNum = accountNum;
        this.userID = userID;
        this.uniqueID = uniqueID;
    }
    
    public String getAccountName() 
    {
        return accountName;
    }

    public void setAccountName(String accountName) 
    {
        this.accountName = accountName;
    }

    public String getAddress() 
    {
        return address;
    }

    public void setAddress(String address) 
    {
        this.address = address;
    }

    public int getAccountNum() 
    {
        return accountNum;
    }

    public void setAccountNum(int accountNum) 
    {
        this.accountNum = accountNum;
    }

    public int getUserID() 
    {
        return userID;
    }

    public void setUserID(int userID) 
    {
        this.userID = userID;
    }
    
    public int getUniqueID() 
    {
        return uniqueID;
    }

    public void setUniqueID(int uniqueID) 
    {
        this.uniqueID = uniqueID;
    }
    
    public void print()
    {
        System.out.println("======================");
        System.out.println("Type: BillPayAccount");
        System.out.println("AccountName: " + accountName);
        System.out.println("AccountNum: " + accountNum);
        System.out.println("UserID: " + userID);
        System.out.println("Address: " + address);
        System.out.println("UniqueID: " + uniqueID);
        System.out.println("======================");
    }
}
