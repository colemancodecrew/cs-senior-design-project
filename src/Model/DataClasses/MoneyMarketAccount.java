package Model.DataClasses;

import java.time.LocalDate;

/**
 * CheckingAccount subclass
 * @author Cozette Napoles
 */
public class MoneyMarketAccount extends BankAccount
{
    
    public MoneyMarketAccount(double balance, double pir, int userID, int accountID,
                                        LocalDate lastDate, LocalDate lastDatePayReceived) 
    {
           super(userID, accountID, balance, pir, lastDate, lastDatePayReceived);
    }
}
