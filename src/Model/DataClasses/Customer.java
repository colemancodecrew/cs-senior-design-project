/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DataClasses;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Manish
 * @author Rob Vary
 */
public class Customer extends User
{
    private BankAccountFactory BAF;
    public ArrayList<BankAccount> accounts;
    public ArrayList<BillPayAccount> bp_accounts;
    public ArrayList<Bill> bills;
    
    public Customer(int userID, String userName, String userPassword, String userType, 
                String lastName, String firstName, String address, String city) 
    {
        super(userID, userName, userPassword, userType, 
                lastName, firstName, address, city);
        BAF = BankAccountFactory.getBankAccountFactory();
        accounts = new ArrayList<>();
        bp_accounts = new ArrayList<>();
        bills = new ArrayList<>();
    }
    
    public void addAccount(BankAccount bankaccount)
    {
        accounts.add(bankaccount);
    }
    
    public BankAccount setupBankAccount(Class<?> classArg)
    {
        LocalDate today = LocalDate.now();
        //default pir
        double pir = (classArg.equals(Model.DataClasses.CreditCardAccount.class)) ? 17.99 : 
                     (classArg.equals(Model.DataClasses.SavingsAccount.class)) ? 2.0 : 
                     (classArg.equals(Model.DataClasses.MoneyMarketAccount.class)) ? 14.99 : 
                     (classArg.equals(Model.DataClasses.HomeMortgageAccount.class)) ? 6.0 : 0.0;
        
        //randomized balance - for simulation
        int balance = (classArg.equals(Model.DataClasses.CreditCardAccount.class)) ? java.util.concurrent.ThreadLocalRandom.current().nextInt(0, 20000) : 
                         (classArg.equals(Model.DataClasses.SavingsAccount.class)) ? java.util.concurrent.ThreadLocalRandom.current().nextInt(0, 5000) : 
                         (classArg.equals(Model.DataClasses.MoneyMarketAccount.class)) ? java.util.concurrent.ThreadLocalRandom.current().nextInt(0, 50000) : 
                         (classArg.equals(Model.DataClasses.HomeMortgageAccount.class)) ? java.util.concurrent.ThreadLocalRandom.current().nextInt(0, 3000000) : 
                         java.util.concurrent.ThreadLocalRandom.current().nextInt(0, 2000);
        
        //if bank account of type classArg does not exist in the database for this customer
        BankAccount ba = BAF.createBankAccount(classArg, balance, pir, userID, 0, today, today);
        return ba;
    }
    
    public boolean addBillPay(BillPayAccount acct)
    {
        for (BillPayAccount bpa : bp_accounts)
        {
            if (bpa.getAccountNum() == acct.getAccountNum())
            {
                return false;
            }
        }
        
        if(!bp_accounts.contains(acct))
        {
            bp_accounts.add(acct);
        }
        
        return true;
    }
    
    public boolean transfer(BankAccount fromAccount, BankAccount toAccount, double theAmount)
    {
       if(fromAccount.getBalance() >= theAmount)
       {
           fromAccount.setBalance(fromAccount.getBalance() - theAmount);
           fromAccount.addTransaction(new Transaction(java.time.LocalDate.now(), "Transfer", "Outgoing Transfer", theAmount, fromAccount.getBalance(), getUserID(), fromAccount.getAccountID(), 0));
           
           boolean neg = false;
           if(toAccount.getClass().equals(HomeMortgageAccount.class) || toAccount.getClass().equals(CreditCardAccount.class))
           {
               neg = true;
           }
           
           toAccount.setBalance(toAccount.getBalance() + ((neg) ? -theAmount : theAmount));
           toAccount.addTransaction(new Transaction(java.time.LocalDate.now(), "Transfer", "Incoming Transfer", theAmount, toAccount.getBalance(), getUserID(), toAccount.getAccountID(), 0));
           return true;
       }
       
       return false;
    }
    
    public BankAccount findBankAccount(int accountID)
    {
        for(BankAccount acct : accounts)
        {
            if(acct.getAccountID() == accountID)
            {
                return acct;
            }
        }
        
        return null;
    }
    
    public BillPayAccount findBillPayAccount(int uniqueID)
    {
        for(BillPayAccount bpa : bp_accounts)
        {
            if(bpa.getUniqueID() == uniqueID)
            {
                return bpa;
            }
        }
        
        return null;
    }
    
    public Bill findBill(int uniqueID)
    {
        for(Bill bill : bills)
        {
            if(bill.getUniqueID() == uniqueID)
            {
                return bill;
            }
        }
        
        return null;
    }
   
    public String generateBillReports()
    {
        String R = "";
        for(Bill B : bills)
        {
            R += "Account Name: ";
            R += B.getAccountName() + "\n";
            R += "Address: ";
            R += B.getAddress() + "\n";
            R += "Account Number: ";
            R += B.getAccountNum() + "\n";
            R += "Total: ";
            R += B.getTotal() + "\n";
            R += "Date Due: ";
            R += B.getDue_date() + "\n";
            R += "\n\n";
        }
    return R;
    }
    
    public boolean payBill(Bill To, BankAccount From, double Amount)
    {
        double amtToDeductFromBill = (Amount >= To.getTotal()) ? To.getTotal() : Amount;
        double amtToDeductFromAcct = (Amount <= To.getTotal()) ? Amount : To.getTotal();
        
        if(amtToDeductFromAcct <= From.getBalance())
        {
            From.setBalance(From.getBalance() - amtToDeductFromAcct);
            To.setTotal(To.getTotal() - amtToDeductFromBill);
            return true;
        }
    
        return false;
    }
    
    public void addBill(Bill bill)
    {
        bills.add(bill);
    }

    public HomeMortgageAccount getHomeMortgage()
    {
        for (BankAccount acct : accounts)
        {
            if (acct.getClass().equals(HomeMortgageAccount.class))
            {
                return (HomeMortgageAccount)acct;
            }
        }
        
        return null;
    }
    
    public CreditCardAccount getCreditCard()
    {
        for (BankAccount acct : accounts)
        {
            if (acct.getClass().equals(CreditCardAccount.class))
            {
                return (CreditCardAccount)acct;
            }
        }
        
        return null;
    }
    
    public boolean hasAccount(java.lang.Class account)
    {
        for(BankAccount acct : accounts)
        {
            if(account.equals(acct.getClass()))
            {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean hasBill(String name)
    {
        for(Bill bill : bills)
        {
            if(bill.getAccountName().equals(name))
            {
                return true;
            }
        }
        
        return false;
    }
}