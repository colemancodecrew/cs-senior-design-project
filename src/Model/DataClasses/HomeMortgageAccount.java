package Model.DataClasses;

import java.time.LocalDate;

/**
 *
 * @author Cozette Napoles
 */
public class HomeMortgageAccount extends BankAccount
{
    
    public HomeMortgageAccount(double balance, double pir, int userID, int accountID,
                                    LocalDate lastDate, LocalDate lastDatePayReceived) 
    {
           super(userID, accountID, balance, pir, lastDate, lastDatePayReceived);
    }
}