/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DataClasses;

/**
 *
 * @author Manish
 * @author Rob Vary
 */
public class User 
{
    protected int userID;
    protected String userName;
    protected String userPassword;
    protected String userType;
    protected String lastName;
    protected String firstName;
    protected String address;
    protected String city;

    public User(int userID, String userName, String userPassword, String userType, 
                String lastName, String firstName, String address, String city) 
    {
        this.userID = userID;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userType = userType;
        this.lastName = lastName;
        this.firstName = firstName;
        this.address = address;
        this.city = city;
    }
        
    /**
     * @return userName
     */
    public String getUserName() 
    {
        return userName;
    }

    /**
     * @param userId User Identifying code - no more than 8 characters
     */
    public void setUserName(String userId) 
    {
        this.userName = userId;
    }


    /**
     * @return userPasswordUser Password
     */
    public String getUserPassword() 
    {
        return userPassword;
    }

    /**
     * @param userPassword User Password
     */
    public void setUserPassword(String userPassword) 
    {
        this.userPassword = userPassword;
    }

    /**
     * @return userType
     */
    public String getUserType() 
    {
        return userType;
    }

    /**
     * @param userType
     */
    public void setUserType(String userType) 
    {
        this.userType = userType;
    }
    
    public int getUserID() 
    {
        return userID;
    }

    public void setUserID(int userID) 
    {
        this.userID = userID;
    }

    public String getLastName() 
    {
        return lastName;
    }

    public void setLastName(String lastName) 
    {
        this.lastName = lastName;
    }

    public String getFirstName() 
    {
        return firstName;
    }

    public void setFirstName(String firstName) 
    {
        this.firstName = firstName;
    }

    public String getAddress() 
    {
        return address;
    }

    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getCity() 
    {
        return city;
    }

    public void setCity(String city) 
    {
        this.city = city;
    }
    
    public void print()
    {
        System.out.println("======================");
        System.out.println("UserID: " + userID);
        System.out.println("Username: " + userName);
        System.out.println("UserPass: " + userPassword);
        System.out.println("UserType: " + userType);
        System.out.println("Last name: " + lastName);
        System.out.println("First name: " + firstName);
        System.out.println("Address: " + address);
        System.out.println("City: " + city);
        System.out.println("======================");
    }
}