/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DataClasses;

import Model.DatabaseAccess.DatabaseUser;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Manish
 * @author Rob Vary
 */
public class Admin extends User
{
    private UserFactory UF;
    private ArrayList<User> Users;
    
    
    public Admin(int userID, String userName, String userPassword, String userType, 
                String lastName, String firstName, String address, String city) 
    {
        super(userID, userName, userPassword, userType, 
                lastName, firstName, address, city);
        UF = UserFactory.getUserFactory();
    }
    
    
    public User createUserAccount(Class<?> classArg, int userID, String userName, String userPassword, String userType, 
                String lastName, String firstName, String address, String city)
    {
        User ua = UF.createUser(classArg, userID, userName, userPassword, userType, lastName, firstName, address, city);
        return ua;
    }
    
    public ArrayList<Object> deleteCustomerAccountLists(Customer cust) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException
    {    
    ArrayList<BankAccount> user_ba = cust.accounts;
    ArrayList<BillPayAccount> user_bpa = cust.bp_accounts;
    
    cust.accounts.clear();
    cust.bp_accounts.clear();
    
    ArrayList<Object> objList = new ArrayList<Object>();
    for(BankAccount k : user_ba)
        {
        objList.add(k);
        }
    for(BillPayAccount z : user_bpa)
        {
        objList.add(z);
        }
    return objList;
    }
}