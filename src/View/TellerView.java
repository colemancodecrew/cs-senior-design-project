/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.DataClasses.BankAccount;
import Model.DataClasses.Customer;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;

/**
 *
 * @author Manish
 */
public class TellerView extends javax.swing.JFrame 
{
    public void resetFocus()
    {
        jTabbedPane_Teller.setSelectedIndex(0);
    }
    
    public JProgressBar getjProgressBar_TellerTransfer() {
        return jProgressBar_TellerTransfer;
    }

    public void setReferencedCust(Customer referencedCust) {
        this.referencedCust = referencedCust;
    }

    public Customer getReferencedCust() {
        return referencedCust;
    }
    private Customer referencedCust;
    
    public int getUserID()
    {
        return (jTextField_UserID.getText().equals("")) ? 0 : Integer.parseInt(jTextField_UserID.getText());
    }
    
    public int getUserID2()
    {
        return (jTextField_UserIDTransfer.getText().equals("")) ? 0 : Integer.parseInt(jTextField_UserIDTransfer.getText());
    }
    
    public String getUserIDStr()
    {
        return jTextField_UserID.getText();
    }
    
    public JProgressBar getjProgressBar_TellerWD() {
        return jProgressBar_TellerWD;
    }
    private final ArrayList<javax.swing.JButton> buttons;
    
    public JButton getjButton_Deposit() {
        return jButton_Deposit;
    }

    public JButton getjButton_Logout1() {
        return jButton_Logout1;
    }

    public JButton getjButton_Logout2() {
        return jButton_Logout2;
    }

    public JButton getjButton_OK() {
        return jButton_OK;
    }

    public JButton getjButton_Transfer() {
        return jButton_Transfer;
    }

    public JButton getjButton_Withdraw() {
        return jButton_Withdraw;
    }

    public JComboBox getjComboBox_TransferFrom() {
        return jComboBox_TransferFrom;
    }

    public JComboBox getjComboBox_TransferTo() {
        return jComboBox_TransferTo;
    }
    
    public void removeButtonListeners()
    {
        for(javax.swing.JButton btn : buttons)
        {
            for (ActionListener AL : btn.getActionListeners())
            {
                btn.removeActionListener(AL);
            }
        }
    }

    /**
     * Creates new form Teller
     */
    public TellerView() {
        initComponents();
        
        //Set the Withdraw Radio box checked
        jRadioButton_Withdraw.setSelected(true);
        
        //Hide the Deposit Type label
        jLabel_DepositType.setVisible(false);
        
        //Hide the Deposit Type Combo box
        jComboBox_DepositType.setVisible(false);
        
        //Hide the Deposit Button
        jButton_Deposit.setEnabled(false);
        
        disableComponents();
        
        buttons = new ArrayList<>();
        buttons.add(jButton_Deposit);
        buttons.add(jButton_Logout1);
        buttons.add(jButton_Logout2);
        buttons.add(jButton_OK);
        buttons.add(jButton_Transfer);
        buttons.add(jButton_Withdraw);
        buttons.add(jButton_OKTransfer);
        
        jProgressBar_TellerWD.setVisible(false);
        jProgressBar_TellerTransfer.setVisible(false);
        referencedCust = null;
        
        jComboBox_TransferFrom.removeAllItems();
        jComboBox_TransferFrom.addItem("Unknown user...");
        jComboBox_TransferTo.removeAllItems();
        jComboBox_TransferTo.addItem("Unknown user...");
    }
    
    public void resetTransfer()
    {
        jComboBox_TransferFrom.removeAllItems();
        jComboBox_TransferFrom.addItem("Unknown user...");
        jComboBox_TransferTo.removeAllItems();
        jComboBox_TransferTo.addItem("Unknown user...");
    }
    
    public void enableComponents()
    {
        jComboBox_WDAccounts.setEnabled(true);
        jTextField_WDAmount.setEnabled(true);
        
        if (jRadioButton_Deposit.isSelected())
        {
            jComboBox_DepositType.setEnabled(true);
            jButton_Deposit.setEnabled(true);
        }
        else
        {
            jButton_Withdraw.setEnabled(true);
        }
    }
    
    public void disableComponents()
    {
        jComboBox_WDAccounts.removeAllItems();
        jComboBox_WDAccounts.setEnabled(false);
        jTextField_WDAmount.setEnabled(false);
        jComboBox_DepositType.setEnabled(false);
        jButton_Deposit.setEnabled(false);
        jButton_Withdraw.setEnabled(false);
    }
    
    public void populateAccounts()
    {
        jComboBox_WDAccounts.removeAllItems();
        
        for (BankAccount acct : referencedCust.accounts)
        {
            if(!acct.getClass().equals(Model.DataClasses.HomeMortgageAccount.class) &&
               !acct.getClass().equals(Model.DataClasses.CreditCardAccount.class))
            {
                jComboBox_WDAccounts.addItem(acct.getReadableAccountType().split(" Account")[0] + " #" + acct.truncatedAcctNum() + " ... (Balance: $" + (new DecimalFormat("#0.00")).format(acct.getBalance()) + ")");
            }
            else
            {
                if (jRadioButton_Deposit.isSelected())
                {
                    jComboBox_WDAccounts.addItem(acct.getReadableAccountType().split(" Account")[0] + " #" + acct.truncatedAcctNum() + " ... (Balance: $" + (new DecimalFormat("#0.00")).format(acct.getBalance()) + ")");
                }
            }
        }
        
        enableComponents();
    }
    
    public void populateTransfer()
    {
        jComboBox_TransferFrom.removeAllItems();
        jComboBox_TransferTo.removeAllItems();
        
        if(referencedCust != null)
        {
            for(BankAccount acct : referencedCust.accounts)
            {
                jComboBox_TransferFrom.addItem(acct.getReadableAccountType().split(" Account")[0] + " #" + acct.truncatedAcctNum() + " ... (Balance: $" + (new DecimalFormat("#0.00")).format(acct.getBalance()) + ")");
                jComboBox_TransferTo.addItem(acct.getReadableAccountType().split(" Account")[0] + " #" + acct.truncatedAcctNum() + " ... (Balance: $" + (new DecimalFormat("#0.00")).format(acct.getBalance()) + ")");
            }
        }
    }
    
    public BankAccount getFromAccount()
    {
        String acctNum = jComboBox_TransferFrom.getSelectedItem().toString().split("#")[1].split(" ")[0];
        acctNum = acctNum.replace(")", "");
        
        return referencedCust.findBankAccount(Integer.parseInt(acctNum));
    }
    
    public BankAccount getToAccount()
    {
        String acctNum = jComboBox_TransferTo.getSelectedItem().toString().split("#")[1].split(" ")[0];
        acctNum = acctNum.replace(")", "");
        
        return referencedCust.findBankAccount(Integer.parseInt(acctNum));
    }
    
    public BankAccount getWDAccount()
    {
        String acctNum = jComboBox_WDAccounts.getSelectedItem().toString().split("#")[1].split(" ")[0];
        acctNum = acctNum.replace(")", "");
        
        return referencedCust.findBankAccount(Integer.parseInt(acctNum));
    }
    
    public double getTransferAmount()
    {
        return Double.parseDouble(jTextField_TransferAmount.getText());
    }
    
    public double getWDAmount()
    {
        return Double.parseDouble(jTextField_WDAmount.getText());
    }
    
    public String getDepositType()
    {
        return jComboBox_DepositType.getSelectedItem().toString();
    }
    
    public void addOKListener(ActionListener e)
    {
        jButton_OK.addActionListener(e);
    }
    
    public void addWithdrawListener(ActionListener e)
    {
        jButton_Withdraw.addActionListener(e);
    }
    
    public void addDepositListener(ActionListener e)
    {
        jButton_Deposit.addActionListener(e);
    }
    
    public void addOKListener2(ActionListener e)
    {
        jButton_OKTransfer.addActionListener(e);
    }
    
    public void addTransferListener(ActionListener e)
    {
        jButton_Transfer.addActionListener(e);
    }
    
    public void addLogout1Listener(ActionListener e)
    {
        jButton_Logout1.addActionListener(e);
    }
    
    public void addLogout2Listener(ActionListener e)
    {
        jButton_Logout2.addActionListener(e);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup_Transaction = new javax.swing.ButtonGroup();
        jTabbedPane_Teller = new javax.swing.JTabbedPane();
        jPanel_Transfer = new javax.swing.JPanel();
        jLabel_TransferFrom = new javax.swing.JLabel();
        jLabel_TransferTo = new javax.swing.JLabel();
        jLabel_TransferAmount = new javax.swing.JLabel();
        jButton_Transfer = new javax.swing.JButton();
        jButton_TCancel = new javax.swing.JButton();
        jTextField_TransferAmount = new javax.swing.JTextField();
        jComboBox_TransferFrom = new javax.swing.JComboBox();
        jComboBox_TransferTo = new javax.swing.JComboBox();
        jButton_Logout1 = new javax.swing.JButton();
        jProgressBar_TellerTransfer = new javax.swing.JProgressBar();
        jLabel_UserID1 = new javax.swing.JLabel();
        jTextField_UserIDTransfer = new javax.swing.JTextField();
        jButton_OKTransfer = new javax.swing.JButton();
        jPanel_Deposit = new javax.swing.JPanel();
        jLabel_AccountNumber = new javax.swing.JLabel();
        jLabel_DepositType = new javax.swing.JLabel();
        jLabel_DepositAmount = new javax.swing.JLabel();
        jButton_Withdraw = new javax.swing.JButton();
        jButton_DCancel = new javax.swing.JButton();
        jComboBox_DepositType = new javax.swing.JComboBox();
        jTextField_WDAmount = new javax.swing.JTextField();
        jButton_Deposit = new javax.swing.JButton();
        jLabel_UserID = new javax.swing.JLabel();
        jTextField_UserID = new javax.swing.JTextField();
        jButton_OK = new javax.swing.JButton();
        jRadioButton_Withdraw = new javax.swing.JRadioButton();
        jRadioButton_Deposit = new javax.swing.JRadioButton();
        jLabel_TransactionType = new javax.swing.JLabel();
        jButton_Logout2 = new javax.swing.JButton();
        jProgressBar_TellerWD = new javax.swing.JProgressBar();
        jComboBox_WDAccounts = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Teller");

        jLabel_TransferFrom.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_TransferFrom.setText("Transfer From :");

        jLabel_TransferTo.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_TransferTo.setText("Transfer To :");

        jLabel_TransferAmount.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_TransferAmount.setText("Transfer Amount :");

        jButton_Transfer.setText("Transfer");

        jButton_TCancel.setText("Cancel");
        jButton_TCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_TCancelActionPerformed(evt);
            }
        });

        jTextField_TransferAmount.setText("0.00");

        jComboBox_TransferFrom.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Checkings Account", "Savings Account", "Money Market Account", "Credit card" }));

        jComboBox_TransferTo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Checkings Account", "Savings Account", "Money Market Account", "Home Mortgage Account", "Credit Card Account" }));

        jButton_Logout1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton_Logout1.setText("Logout");

        jLabel_UserID1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_UserID1.setText("User ID :");

        jButton_OKTransfer.setText("Find");

        javax.swing.GroupLayout jPanel_TransferLayout = new javax.swing.GroupLayout(jPanel_Transfer);
        jPanel_Transfer.setLayout(jPanel_TransferLayout);
        jPanel_TransferLayout.setHorizontalGroup(
            jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_TransferLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton_Logout1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53))
            .addGroup(jPanel_TransferLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_TransferLayout.createSequentialGroup()
                        .addComponent(jButton_Transfer, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(jButton_TCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel_TransferLayout.createSequentialGroup()
                        .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel_TransferFrom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel_TransferTo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel_TransferAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(jLabel_UserID1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField_TransferAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel_TransferLayout.createSequentialGroup()
                                .addComponent(jTextField_UserIDTransfer, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton_OKTransfer, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(jProgressBar_TellerTransfer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jComboBox_TransferFrom, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBox_TransferTo, javax.swing.GroupLayout.Alignment.LEADING, 0, 218, Short.MAX_VALUE)))))
                .addContainerGap(198, Short.MAX_VALUE))
        );
        jPanel_TransferLayout.setVerticalGroup(
            jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_TransferLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_TransferLayout.createSequentialGroup()
                        .addComponent(jButton_Logout1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_TransferLayout.createSequentialGroup()
                        .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jProgressBar_TellerTransfer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel_UserID1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextField_UserIDTransfer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton_OKTransfer, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)))
                .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_TransferFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox_TransferFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_TransferTo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox_TransferTo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_TransferAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_TransferAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(114, 114, 114)
                .addGroup(jPanel_TransferLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_Transfer, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_TCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(145, Short.MAX_VALUE))
        );

        jTabbedPane_Teller.addTab("Transfer", jPanel_Transfer);

        jLabel_AccountNumber.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_AccountNumber.setText("Account :");

        jLabel_DepositType.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_DepositType.setText("Deposit Type :");

        jLabel_DepositAmount.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_DepositAmount.setText("Amount :");

        jButton_Withdraw.setText("Withdraw");

        jButton_DCancel.setText("Cancel");
        jButton_DCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_DCancelActionPerformed(evt);
            }
        });

        jComboBox_DepositType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cash", "Personal Check", "Paycheck", "Money Order" }));

        jTextField_WDAmount.setText("0.00");

        jButton_Deposit.setText("Deposit");

        jLabel_UserID.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_UserID.setText("User ID :");

        jButton_OK.setText("Find");

        buttonGroup_Transaction.add(jRadioButton_Withdraw);
        jRadioButton_Withdraw.setText("Withdraw");
        jRadioButton_Withdraw.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton_WithdrawActionPerformed(evt);
            }
        });

        buttonGroup_Transaction.add(jRadioButton_Deposit);
        jRadioButton_Deposit.setText("Deposit");
        jRadioButton_Deposit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton_DepositActionPerformed(evt);
            }
        });

        jLabel_TransactionType.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel_TransactionType.setText("Transaction Type :");

        jButton_Logout2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton_Logout2.setText("Logout");

        jComboBox_WDAccounts.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel_DepositLayout = new javax.swing.GroupLayout(jPanel_Deposit);
        jPanel_Deposit.setLayout(jPanel_DepositLayout);
        jPanel_DepositLayout.setHorizontalGroup(
            jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_DepositLayout.createSequentialGroup()
                .addGap(84, 84, 84)
                .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton_Deposit, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(jLabel_DepositAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(jLabel_DepositType, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(jLabel_AccountNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(jLabel_UserID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel_TransactionType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_DepositLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButton_Withdraw, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton_DCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel_DepositLayout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_DepositLayout.createSequentialGroup()
                                .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextField_UserID, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                                    .addComponent(jComboBox_DepositType, 0, 199, Short.MAX_VALUE)
                                    .addComponent(jTextField_WDAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jRadioButton_Deposit)
                                    .addComponent(jComboBox_WDAccounts, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(40, 40, 40)
                                .addComponent(jButton_OK, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(41, 41, 41)
                                .addComponent(jProgressBar_TellerWD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_DepositLayout.createSequentialGroup()
                                .addComponent(jRadioButton_Withdraw)
                                .addGap(405, 405, 405)
                                .addComponent(jButton_Logout2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(53, 53, 53))
        );
        jPanel_DepositLayout.setVerticalGroup(
            jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_DepositLayout.createSequentialGroup()
                .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_DepositLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jRadioButton_Withdraw)
                        .addGap(9, 9, 9)
                        .addComponent(jRadioButton_Deposit))
                    .addGroup(jPanel_DepositLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel_TransactionType, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel_DepositLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton_Logout2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(37, 37, 37)
                .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_DepositLayout.createSequentialGroup()
                        .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel_UserID, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_UserID, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton_OK, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel_AccountNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                            .addComponent(jComboBox_WDAccounts)))
                    .addComponent(jProgressBar_TellerWD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_DepositAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_WDAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_DepositType, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox_DepositType, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(jPanel_DepositLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_Deposit, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_Withdraw, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_DCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(142, Short.MAX_VALUE))
        );

        jTabbedPane_Teller.addTab("Withdraw/Deposit", jPanel_Deposit);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane_Teller)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane_Teller)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioButton_DepositActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton_DepositActionPerformed
        
        /*If the Deposit Radio button is selected, show the 
          Deposit Type Label, Deposit Type Combo box and Deposit Button*/
        if (jRadioButton_Deposit.isSelected())
        {
            jLabel_DepositType.setVisible(true);
            jComboBox_DepositType.setVisible(true);
            jButton_Deposit.setEnabled(true);
            jButton_Withdraw.setEnabled(false);
            referencedCust = null;
            disableComponents();
        }
    }//GEN-LAST:event_jRadioButton_DepositActionPerformed

    private void jRadioButton_WithdrawActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton_WithdrawActionPerformed
        
        /*If the Withdraw Radio button is selected, hide the 
          Deposit Type Label, Deposit Type Combo box and Deposit Button*/
        if (jRadioButton_Withdraw.isSelected())
        {
            jLabel_DepositType.setVisible(false);
            jComboBox_DepositType.setVisible(false);
            jButton_Deposit.setEnabled(false);
            jButton_Withdraw.setEnabled(true);
            referencedCust = null;
            disableComponents();
        }
    }//GEN-LAST:event_jRadioButton_WithdrawActionPerformed

    private void jButton_DCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_DCancelActionPerformed
        
        //Clear the input fields
        jTextField_UserID.setText("");
        jTextField_WDAmount.setText("");
        referencedCust = null;
        disableComponents();
    }//GEN-LAST:event_jButton_DCancelActionPerformed

    private void jButton_TCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_TCancelActionPerformed

        //Clear the input fields
        jTextField_TransferAmount.setText("");
    }//GEN-LAST:event_jButton_TCancelActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TellerView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TellerView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TellerView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TellerView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TellerView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup_Transaction;
    private javax.swing.JButton jButton_DCancel;
    private javax.swing.JButton jButton_Deposit;
    private javax.swing.JButton jButton_Logout1;
    private javax.swing.JButton jButton_Logout2;
    private javax.swing.JButton jButton_OK;
    private javax.swing.JButton jButton_OKTransfer;
    private javax.swing.JButton jButton_TCancel;
    private javax.swing.JButton jButton_Transfer;
    private javax.swing.JButton jButton_Withdraw;
    private javax.swing.JComboBox jComboBox_DepositType;
    private javax.swing.JComboBox jComboBox_TransferFrom;
    private javax.swing.JComboBox jComboBox_TransferTo;
    private javax.swing.JComboBox<String> jComboBox_WDAccounts;
    private javax.swing.JLabel jLabel_AccountNumber;
    private javax.swing.JLabel jLabel_DepositAmount;
    private javax.swing.JLabel jLabel_DepositType;
    private javax.swing.JLabel jLabel_TransactionType;
    private javax.swing.JLabel jLabel_TransferAmount;
    private javax.swing.JLabel jLabel_TransferFrom;
    private javax.swing.JLabel jLabel_TransferTo;
    private javax.swing.JLabel jLabel_UserID;
    private javax.swing.JLabel jLabel_UserID1;
    private javax.swing.JPanel jPanel_Deposit;
    private javax.swing.JPanel jPanel_Transfer;
    private javax.swing.JProgressBar jProgressBar_TellerTransfer;
    private javax.swing.JProgressBar jProgressBar_TellerWD;
    private javax.swing.JRadioButton jRadioButton_Deposit;
    private javax.swing.JRadioButton jRadioButton_Withdraw;
    private javax.swing.JTabbedPane jTabbedPane_Teller;
    private javax.swing.JTextField jTextField_TransferAmount;
    private javax.swing.JTextField jTextField_UserID;
    private javax.swing.JTextField jTextField_UserIDTransfer;
    private javax.swing.JTextField jTextField_WDAmount;
    // End of variables declaration//GEN-END:variables
}
